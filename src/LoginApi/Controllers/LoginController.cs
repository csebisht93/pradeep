﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoginApi.Business;
using LoginApi.Context;
using LoginApi.Entities;
 using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace LoginApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        ILoggerFactory _loggerFactory;
        //private readonly IConfiguration Configuration;
        //private readonly db_context _context;
        //public LoginController(db_context context)
        //{
        //    _context = context;
        //}
        public LoginController(ILoggerFactory loggerFactory)
        {
            _loggerFactory = loggerFactory;
        }
        BusinessFacade objBusinessFacade = new BusinessFacade();
        // GET api/values
        [HttpGet]
        public List<Country> GetCountry()
        {
            // db_context context = HttpContext.RequestServices.GetService(typeof(Models.User)) as db_context;    
            var logger = _loggerFactory.CreateLogger("LoginCategory");
            logger.LogInformation("Calling the GetCountry action");
            return objBusinessFacade.GetCountry();
           //return new User { context.GetAllUser };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
