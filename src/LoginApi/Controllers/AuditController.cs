﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoginApi.Business;
using LoginApi.Entities;
using LoginApi.Entities.Baseclasses;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LoginApi.Controllers
{
    
    [Route("Login-Api/[controller]/[action]")]
    [ApiController]
    public class AuditController : ControllerBase
    {
        BusinessFacade objBusinessFacade = new BusinessFacade();

        #region insert Partner
        public Response<AuditTb> InsertAuditTb(AuditTb pobjAuditTb)
        {
            Response<AuditTb> response = new Response<AuditTb>();
            try
            {
                response = objBusinessFacade.InsertAuditTb(pobjAuditTb);
            }
            catch (Exception ex)
            {
              
            }
            return response;
        }
        #endregion
    }
}