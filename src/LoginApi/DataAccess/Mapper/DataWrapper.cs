﻿using LoginApi.DataAccess.Constants;
using LoginApi.Entities;
using LoginApi.Entities.Baseclasses;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace LoginApi.DataAccess.Mapper
{

    public static class DataWrapper
    {
       
        static ILoggerFactory _loggerFactory;

        #region Common

        public static ServiceResponse MapResponsecountry(DataSet pds)
        {
            ServiceResponse response = new ServiceResponse();
            if (pds != null && pds.Tables.Count > 0 && pds.Tables[0].Rows.Count > 0)
            {
                switch (Convert.ToInt32(pds.Tables[0].Rows[0][DBFields.ErrCode]))
                {
                    case 0:
                        response.Errcode = 1;
                        break;
                    default:
                        response.Errcode = Convert.ToInt32(pds.Tables[0].Rows[0][DBFields.ErrCode]);
                        break;
                }
                response.Id = Convert.ToInt32(pds.Tables[0].Rows[0][DBFields.Id]);
            }
            else
            {
               // response.Errcode = Convert.ToInt32(HelperConstants.ErrorCode.InternalError);
            }
            return response;
        }
        #endregion
        public static ServiceResponse MapResponse(int result)
        {
            ServiceResponse response = new ServiceResponse();
            if (result > 0)
            {
                switch (result)
                {
                    case 0:
                        response.Errcode = result;
                        break;
                    default:
                        response.Errcode = 200;
                        break;
                }
                response.Id = result;
            }
            else
            {
                response.Errcode = 500;
                // response.Errcode = Convert.ToInt32(HelperConstants.ErrorCode.InternalError);
            }
            return response;
        }

        #region Get Country
        public static List<Country> MapGetCountry(DataSet Ds)
        {
            List<Country> lobjCountry = new List<Country>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCountry = new List<Country>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        Country objectCountry = new Country()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            Name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.CountryName]),

                        };
                        lobjCountry.Add(objectCountry);
                    }
                }
            }
            catch (Exception ex)
            {              
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjCountry;
        }
        #endregion

        #region Get Country
        public static List<State> MapGetState(DataSet Ds)
        {
            List<State> lobjState = new List<State>(); 
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjState = new List<State>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        State objectState = new State()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            CountryId= Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CountryId]),
                            Name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.StateName]),
                            country_name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.CountryName])

                        };
                        lobjState.Add(objectState);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjState;
        }
        #endregion

        #region Get Country
        public static List<City> MapGetCity(DataSet Ds)
        {
            List<City> lobjCity = new List<City>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCity = new List<City>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        City objectCity = new City()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            Name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.CityName]),
                            StateId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.StateId]),
                            CountryId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CountryId]),
                            state_name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.StateName]),
                            country_name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.CountryName]),

                        };
                        lobjCity.Add(objectCity);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjCity;
        }
        #endregion

        #region Get PartnerEdit
        public static List<Partner> MapGetPartnerDetails(DataSet Ds)
        {
            List<Partner> lobjPartner = null;
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjPartner = new List<Partner>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        Partner objectPartner = new Partner()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            PartnerIdNumber = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.PartnerIdNumber]),
                            CompanyName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.CompanyName]),
                            Address = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Address]),
                            Pincode = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Pincode]),
                            CountryId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CountryId]),
                            StateId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.StateId]),
                            CityId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CityId]),
                            IsActive = Convert.ToBoolean(Ds.Tables[0].Rows[i][DBFields.IsActive]),

                        };
                        lobjPartner.Add(objectPartner);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjPartner;
        }
        #endregion

        #region Get PartnercontactList
        public static List<PartnerContactDetails> MapGetPartnerContactList(DataSet Ds)
        {
            List<PartnerContactDetails> lobjPartner = new List<PartnerContactDetails>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjPartner = new List<PartnerContactDetails>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        PartnerContactDetails objectPartner = new PartnerContactDetails()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            UserName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.UserName]),
                            Password = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Password]),
                            FirstName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.FirstName]),
                            MiddleName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.MiddleName]),
                            LastName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.LastName]),
                            ContactNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ContactNo]),
                            Email = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Email]),
                            DepartmentName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.DepartmentName]),
                            Designation = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Designation]),
                            IsActive = Convert.ToBoolean(Ds.Tables[0].Rows[i][DBFields.IsActive]),
                            UserId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.UserId]),
                        };
                        lobjPartner.Add(objectPartner);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjPartner;
        }
        #endregion

        #region Get Partner ProductList
        public static List<PartnerProductDetails> MapGetPartnerProductList(DataSet Ds)
        {
            List<PartnerProductDetails> lobjPartner = new List<PartnerProductDetails>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjPartner = new List<PartnerProductDetails>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        PartnerProductDetails objectPartner = new PartnerProductDetails()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            ProductId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.ProductId]),
                            IsActive = Convert.ToBoolean(Ds.Tables[0].Rows[i][DBFields.IsActive]),

                        };
                        lobjPartner.Add(objectPartner);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjPartner;
        }
        #endregion

        #region Get ProductList
        public static List<Product> MapGetProductList(DataSet Ds)
        {
            List<Product> lobjPartner = new List<Product>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjPartner = new List<Product>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        Product objectPartner = new Product()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            product_id = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ProductId]),
                            product_name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ProductName]),
                             Variant = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Variant]),
                            description = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.description]),
                        };
                        lobjPartner.Add(objectPartner);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjPartner;
        }
        #endregion

        #region Get Product by id
        public static List<Product> MapGetProductById(DataSet Ds)
        {
            List<Product> lobjProduct = null;
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjProduct = new List<Product>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        Product objectProduct = new Product()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            product_id = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ProductId]),
                            product_name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ProductName]),
                            description = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.description])

                        };
                        lobjProduct.Add(objectProduct);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjProduct;
        }
        #endregion

        #region Get CustomerSupport List
        public static List<CustomerSupport> MapGetCustomerSupportList(DataSet Ds)
        {
            List<CustomerSupport> lobjCustomerSupport = new List<CustomerSupport>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCustomerSupport = new List<CustomerSupport>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        CustomerSupport objectCustomerSupport = new CustomerSupport()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            CustomerSupportId = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.CustomerSupportId]),
                            BranchName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.BranchName]),
                            Address = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Address]),
                            Pincode = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Pincode]),

                        };
                        lobjCustomerSupport.Add(objectCustomerSupport);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjCustomerSupport;
        }
        #endregion


        //For edit
        #region Get CustomerSupportEdit
        public static List<CustomerSupport> MapGetCustomerSupportDetails(DataSet Ds)
        {
            List<CustomerSupport> lobjCustomerSupport = null;
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCustomerSupport = new List<CustomerSupport>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        CustomerSupport objectCustomerSupport = new CustomerSupport()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            CustomerSupportId = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.CustomerSupportId]),
                            CoampanyId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CompanyId]),
                            BranchName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.BranchName]),
                            Address = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Address]),
                            Pincode = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Pincode]),
                            CountryId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CountryId]),
                            StateId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.StateId]),
                            CityId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CityId]),
                            IsActive = Convert.ToBoolean(Ds.Tables[0].Rows[i][DBFields.IsActive]),
                           
                        };
                        lobjCustomerSupport.Add(objectCustomerSupport);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjCustomerSupport;
        }
        #endregion

        #region Get SupportEmployeeList
        public static List<CustomerSupportEmployee> MapGetCustomerSupportEmployeeList(DataSet Ds)
        {
            List<CustomerSupportEmployee> lobjSupportEmployee = new List<CustomerSupportEmployee>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjSupportEmployee = new List<CustomerSupportEmployee>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        CustomerSupportEmployee objectSupportEmployee = new CustomerSupportEmployee()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            UserName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.UserName]),
                            Password = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Password]),
                            FirstName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.FirstName]),
                            MiddleName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.MiddleName]),
                            LastName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.LastName]),
                            ContactNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ContactNo]),
                            Email = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Email]),
                            DepartmentName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.DepartmentName]),
                            Designation = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Designation]),
                            IsActive = Convert.ToBoolean(Ds.Tables[0].Rows[i][DBFields.IsActive]),
                            UserId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.UserId]),
                        };
                        lobjSupportEmployee.Add(objectSupportEmployee);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjSupportEmployee;
        }
        #endregion

        #region Get Partner ProductList
        public static List<CustomerSupportProduct> MapGetCustomerSupportProductList(DataSet Ds)
        {
            List<CustomerSupportProduct> lobjSupport = new List<CustomerSupportProduct>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjSupport = new List<CustomerSupportProduct>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        CustomerSupportProduct objectSupport = new CustomerSupportProduct()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            ProductId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.ProductId]),
                            IsActive = Convert.ToBoolean(Ds.Tables[0].Rows[i][DBFields.IsActive]),

                        };
                        lobjSupport.Add(objectSupport);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjSupport;
        }
        #endregion

        #region Get PartnerList
        public static List<Partner> MapGetPartnerList(DataSet Ds)
        {
            List<Partner> lobjPartner = new List<Partner>(); 
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjPartner = new List<Partner>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        Partner objectPartner = new Partner()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            PartnerIdNumber = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.PartnerIdNumber]),
                            CompanyName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.CompanyName]),
                            Address = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Address]),
                            Pincode = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Pincode]),

                        };
                        lobjPartner.Add(objectPartner);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjPartner;
        }
        #endregion

        #region Company section

        #region Customerlist
        public static List<Company> MapGetCompanyList(DataSet Ds)
        {
            List<Company> lobjCompany = new List<Company>(); ;
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCompany = new List<Company>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        Company objectCompany = new Company()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            CompanyName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.CompanyName]),
                            CompanyCode = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.CompanyCode]),
                            Email = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Email]),
                            Website = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Website]),
                            ContactNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ContactNo]),


                        };
                        lobjCompany.Add(objectCompany);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjCompany;
        }
        #endregion

        #region Company edit
        public static List<Company> MapGetCompanyDetailsById(DataSet Ds)
        {
            List<Company> lobjCompany = null;
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCompany = new List<Company>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        Company objectCompany = new Company()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            CompanyName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.CompanyName]),
                            CompanyCode = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.CompanyCode]),
                            Email = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Email]),
                            Website = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Website]),
                            ContactNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ContactNo]),
                            Address = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Address]),
                            CountryId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CountryId]),
                            StateId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.StateId]),
                            CityId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CityId]),
                            Pincode = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Pincode]),
                            TinNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.TinNo]),
                            TanNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.TanNo]),
                            CinNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.CinNo]),
                            GstNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.GstNo]),
                            CompanyLogo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.CompanyLogo]),

                        };
                        lobjCompany.Add(objectCompany);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjCompany;
        }
        #endregion

        #endregion

        #region Branch section
        #region Branch list
        public static List<Branch> MapGetBranchList(DataSet Ds)
        {
            List<Branch> lobjBranch = new List<Branch>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjBranch = new List<Branch>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        Branch objectBranch = new Branch()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            CompanyName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.CompanyName]),
                            BranchName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.BranchName]),
                            BranchCode = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.BranchCode]),
                            ContactNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ContactNo]),

                        };
                        lobjBranch.Add(objectBranch);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjBranch;
        }
        #endregion

        #region Branch edit
        public static List<Branch> MapGetBranchDetailsById(DataSet Ds)
        {
            List<Branch> lobjBranch = null;
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjBranch = new List<Branch>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        Branch objectBranch = new Branch()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            CompanyId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CompanyId]),
                            BranchName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.BranchName]),
                            BranchCode = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.BranchCode]),
                            ContactNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ContactNo]),
                            Address = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Address]),
                            CountryId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CountryId]),
                            StateId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.StateId]),
                            CityId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CityId]),
                            Pincode = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Pincode]),

                        };
                        lobjBranch.Add(objectBranch);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjBranch;
        }
        #endregion

        #endregion

        #region Department section
        #region Department list
        public static List<Department> MapGetDepartmentList(DataSet Ds)
        {
            List<Department> lobjDepartment = new List<Department>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjDepartment = new List<Department>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        Department objectDepartment = new Department()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            CompanyName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.CompanyName]),
                            DepartmentName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.DepartmentName]),
                            DepartmentCode = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.DepartmentCode]),

                        };
                        lobjDepartment.Add(objectDepartment);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjDepartment;
        }
        #endregion

        #region Department edit
        public static List<Department> MapGetDepartmentDetailsById(DataSet Ds)
        {
            List<Department> lobjDepartment = null;
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjDepartment = new List<Department>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        Department objectDepartment = new Department()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            CompanyId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CompanyId]),
                            DepartmentName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.DepartmentName]),
                            DepartmentCode = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.DepartmentCode]),

                        };

                        lobjDepartment.Add(objectDepartment);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjDepartment;
        }
        #endregion

        #endregion
        #region Designation section
        #region Designation list
        public static List<Designation> MapGetDesignationList(DataSet Ds)
        {
            List<Designation> lobjDesignation = new List<Designation>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjDesignation = new List<Designation>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        Designation objectDesignation = new Designation()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            DesignationName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Designation]),

                        };
                        lobjDesignation.Add(objectDesignation);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjDesignation;
        }
        #endregion

        #region Designation edit
        public static List<Designation> MapGetDesignationDetailsById(DataSet Ds)
        {
            List<Designation> lobjDesignation = null;
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjDesignation = new List<Designation>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        Designation objectDesignation = new Designation()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            DesignationName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Designation]),

                        };

                        lobjDesignation.Add(objectDesignation);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjDesignation;
        }
        #endregion

        #endregion

        #region Employee section
        #region Employee list
        public static List<Employee> MapGetEmployeeList(DataSet Ds)
        {
            List<Employee> lobjEmployee = new List<Employee>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjEmployee = new List<Employee>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        Employee objectEmployee = new Employee()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            FirstName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.FirstName]),
                            EmployeeCode = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.EmployeeCode]),
                            Email = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Email]),
                            ContactNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ContactNo]),
                            Address = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Address]),


                        };
                        lobjEmployee.Add(objectEmployee);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjEmployee;
        }
        #endregion

        #region Employee edit
        public static List<Employee> MapGetEmployeeDetailsById(DataSet Ds)
        {
            List<Employee> lobjEmployee = null;
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjEmployee = new List<Employee>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        Employee objectEmployee = new Employee()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            Title = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Title]),
                            FirstName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.FirstName]),
                            MiddleName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.MiddleName]),
                            LastName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.LastName]),
                            UserName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.UserName]),
                            Password = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Password]),
                            EmployeeCode = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.EmployeeCode]),
                            CompanyId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CompanyId]),

                            DepartmentId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.DepartmentId]),
                            BranchId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.BranchId]),
                            DesignationId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.DesignationId]),
                            RoleId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.RoleId]),
                            FatherName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.FatherName]),
                            GenderId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.GenderId]),
                            DateOfBirth = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.DateOfBirth]),
                            MaritalStatus = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.MaritalStatus]),
                            UserId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.UserId]),
                            Email = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Email]),
                            ContactNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ContactNo]),
                            Address = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Address]),
                            CountryId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CountryId]),
                            StateId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.StateId]),
                            CityId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CityId]),
                            Pincode = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Pincode]),
                            Photograph = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Photograph]),


                        };
                        lobjEmployee.Add(objectEmployee);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjEmployee;
        }
        #endregion

        #endregion


        #region Login
        public static List<UserLogin> MapGetUserDetails(DataSet Ds)
        {
            List<UserLogin> lobjUserlogin = new List<UserLogin>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjUserlogin = new List<UserLogin>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        UserLogin objectUserLogin = new UserLogin()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            UserName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.UserName]),
                            Password = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Password]),
                            UserType = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.UserType]),
                            Salt = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Salt]),
                            LoginDateTime = Convert.ToDateTime(Ds.Tables[0].Rows[i][DBFields.LoginDateTime]),
                            LogOutDateTime = Convert.ToDateTime(Ds.Tables[0].Rows[i][DBFields.LogOutDateTime]),

                        };
                        lobjUserlogin.Add(objectUserLogin);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjUserlogin;
        }
        #endregion

        public static ServiceResponse PartnerMapResponse(DataSet pds)
        {
            ServiceResponse response = new ServiceResponse();
            if (pds != null && pds.Tables.Count > 0 && pds.Tables[0].Rows.Count > 0)
            {

                response.Id = Convert.ToInt32(pds.Tables[0].Rows[0][DBFields.Id]);
                response.Errcode = Convert.ToInt32(200);
            }
            else
            {
                 response.Errcode = Convert.ToInt32(400);
            }
            return response;
        }
        #region Role section
        #region Role list
        public static List<Role> MapGetRoleList(DataSet Ds)
        {
            List<Role> lobjRole = new List<Role>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjRole = new List<Role>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        Role objectRole = new Role()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            RoleName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.RoleName]),
                            RoleDescription = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.RoleDescription]),
                        };
                        lobjRole.Add(objectRole);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjRole;
        }
        #endregion

        #region Role edit
        public static List<Role> MapGetRoleDetailsById(DataSet Ds)
        {
            List<Role> lobjRole = null;
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjRole = new List<Role>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        Role objectRole = new Role()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            RoleName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.RoleName]),
                            RoleDescription = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.RoleDescription]),

                        };

                        lobjRole.Add(objectRole);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjRole;
        }
        #endregion

        #endregion
        #region ProductVariant

        #region Get ProductVariantList
        public static List<ProductVariant> MapGetProductVariantList(DataSet Ds)
        {
            List<ProductVariant> lobjPartner = new List<ProductVariant>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjPartner = new List<ProductVariant>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        ProductVariant objectPartner = new ProductVariant()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            product_id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.ProductId]),
                            variant_code = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.VariantCode]),
                            Variant = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Variant]),
                            description = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.description]),
                            product_name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ProductName]),
                            product_type_id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.ProductTypeId]),
                        };
                        lobjPartner.Add(objectPartner);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjPartner;
        }
        #endregion

        #region Get ProductVariant by id
        public static List<ProductVariant> MapGetProductVariantById(DataSet Ds)
        {
            List<ProductVariant> lobjProduct = null;
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjProduct = new List<ProductVariant>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        ProductVariant objectProduct = new ProductVariant()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            product_id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.ProductId]),
                            variant_code = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.VariantCode]),
                            Variant = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Variant]),

                        };
                        lobjProduct.Add(objectProduct);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjProduct;
        }
        #endregion

        #endregion

        public static Response<AuditTb> InsertAuditResponse(DataSet pds)
        {
            Response<AuditTb> response = new Response<AuditTb>();
            if (pds != null && pds.Tables.Count > 0 && pds.Tables[0].Rows.Count > 0)
            {

                response.Error = Convert.ToString(200);
            }
            else
            {
                response.Error = Convert.ToString(400);
            }
            return response;
        }
        #region Get ProductCustomerCount
        public static List<ReportCarrier> MapProductCustomerCount(DataSet Ds)
        {
            List<ReportCarrier> lobjCountry = new List<ReportCarrier>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCountry = new List<ReportCarrier>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        ReportCarrier objectCountry = new ReportCarrier()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            CustomerCount = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CustomerCount]),
                            Variant_name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Variant]),
                            Variant_Code = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.VariantCode]),

                        };
                        lobjCountry.Add(objectCountry);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjCountry;
        }
        #endregion
        public static List<ProductVariant> MapGetProductListNew(DataSet Ds)
        {
            List<ProductVariant> lobjPartner = new List<ProductVariant>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjPartner = new List<ProductVariant>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        ProductVariant objectPartner = new ProductVariant()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            product_code = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ProductId]),
                            product_name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ProductName]),
                            Variant = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Variant]),
                            description = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.description]),
                            CreatedBy = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CreatedBy]),
                            CreatedOn = Convert.ToDateTime(Ds.Tables[0].Rows[i][DBFields.CreatedOn]),
                        };
                        lobjPartner.Add(objectPartner);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjPartner;
        }


        #region Login
        public static dynamic InactiveLoginAPIMethod(DataSet Ds)
        {
            dynamic dynamics = "";
            try
            {               
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                   
                    dynamics = "Delete  Successfully";
                }
                else
                {
                    dynamics= "Delete Operation Fail";
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return dynamics;
        }
        #endregion
    }
}
