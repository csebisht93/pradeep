﻿using LoginApi.Context;
using LoginApi.DataAccess.Constants;
using LoginApi.Entities;
using LoginApi.Entities.Baseclasses;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace LoginApi.DataAccess.Wrapper
{

    public  class SPWrapper  
    {
        static ILoggerFactory _loggerFactory;
        public static string ConnectionString { get; set; }

        public SPWrapper(string connectionString)
        {
            ConnectionString = connectionString;
        }

        static MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }

       

        public static DataSet GetCountry()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();//new MySqlConnection(GetConnectionString());
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.CountryGet, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                //cmd.Parameters.AddWithValue(DBFields.Id, PoNumber);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
                // _logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        public static DataSet GetState()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();//new MySqlConnection(GetConnectionString());
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.StateGet, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                //cmd.Parameters.AddWithValue(DBFields.Id, PoNumber);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
                // _logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        public static DataSet GetCity()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();//new MySqlConnection(GetConnectionString());
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.CityGet, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                //cmd.Parameters.AddWithValue(DBFields.Id, PoNumber);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
                // _logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }

        #region save country
        public static int InsertCountry(Country pobjCountry)
        {
            // DataSet ds = new DataSet();
            int result = 0;
           // MySqlDataAdapter da;
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertCountry, con);

            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, pobjCountry.Id);
                cmd.Parameters.AddWithValue(DBFields.CountryName, pobjCountry.Name);
                cmd.Parameters.AddWithValue(DBFields.CreatedBy, pobjCountry.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, pobjCountry.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, pobjCountry.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, pobjCountry.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, pobjCountry.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, pobjCountry.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, pobjCountry.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, pobjCountry.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, pobjCountry.IsActive);

                result= cmd.ExecuteNonQuery();
               // da = new MySqlDataAdapter(cmd);
               // da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save country SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return result;
        }

        #endregion

        #region save state
        public static int InsertState(State pobjState)
        {
            int result = 0;
           // MySqlDataAdapter da;
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertState, con);

            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, pobjState.Id);
                cmd.Parameters.AddWithValue(DBFields.CountryId, pobjState.CountryId);
                cmd.Parameters.AddWithValue(DBFields.StateName, pobjState.Name);
                cmd.Parameters.AddWithValue(DBFields.CreatedBy, pobjState.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, pobjState.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, pobjState.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, pobjState.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, pobjState.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, pobjState.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, pobjState.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, pobjState.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, pobjState.IsActive);


                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save state SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return result;
        }

        #endregion

        #region save city
        public static int InsertCity(City pobjCity)
        {
            int result = 0;
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertCity, con);

            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, pobjCity.Id);
                cmd.Parameters.AddWithValue(DBFields.CountryId, pobjCity.CountryId);
                cmd.Parameters.AddWithValue(DBFields.StateId, pobjCity.StateId);
                cmd.Parameters.AddWithValue(DBFields.CityName, pobjCity.Name);
                cmd.Parameters.AddWithValue(DBFields.CreatedBy, pobjCity.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, pobjCity.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, pobjCity.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, pobjCity.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, pobjCity.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, pobjCity.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, pobjCity.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, pobjCity.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, pobjCity.IsActive);


                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save state SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return result;
        }

        #endregion

        //#region save Partner
        //public static int InsertPartner(Partner pobjPartner)
        //{
        //    int result = 0;
        // //   MySqlDataAdapter da;
        //    MySqlConnection con = GetConnection();
        //    MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertPartner, con);
        //    XMLCreater obXMLCreater = new XMLCreater();
        //    try
        //    {
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Connection.Open();

        //        cmd.Parameters.AddWithValue(DBFields.Id, pobjPartner.Id);
        //        cmd.Parameters.AddWithValue(DBFields.PartnerIdNumber, pobjPartner.PartnerIdNumber);
        //        cmd.Parameters.AddWithValue(DBFields.CompanyName, pobjPartner.CompanyName);
        //        cmd.Parameters.AddWithValue(DBFields.Address, pobjPartner.Address);
        //        cmd.Parameters.AddWithValue(DBFields.CountryId, pobjPartner.CountryId);
        //        cmd.Parameters.AddWithValue(DBFields.StateId, pobjPartner.StateId);
        //        cmd.Parameters.AddWithValue(DBFields.CityId, pobjPartner.CityId);
        //        cmd.Parameters.AddWithValue(DBFields.Pincode, pobjPartner.Pincode);
        //        cmd.Parameters.AddWithValue(DBFields.PartnerContactDetailXml, obXMLCreater.GetPartnerContactXml(pobjPartner.PartnerContactDetailXml));
        //        cmd.Parameters.AddWithValue(DBFields.PartnerProductDetailXml, obXMLCreater.GetPartnerProductXml(pobjPartner.PartnerProductDetailXml));

        //        cmd.Parameters.AddWithValue(DBFields.CreatedBy, pobjPartner.CreatedBy);
        //        cmd.Parameters.AddWithValue(DBFields.CreatedOn, pobjPartner.CreatedOn);
        //        cmd.Parameters.AddWithValue(DBFields.ModifiedBy, pobjPartner.ModifiedBy);
        //        cmd.Parameters.AddWithValue(DBFields.ModifiedOn, pobjPartner.ModifiedOn);
        //        cmd.Parameters.AddWithValue(DBFields.CreatedIP, pobjPartner.CreatedIP);
        //        cmd.Parameters.AddWithValue(DBFields.ModifiedIP, pobjPartner.ModifiedIP);
        //        cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, pobjPartner.CreatedMacAddress);
        //        cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, pobjPartner.ModifiedMacAddress);
        //        cmd.Parameters.AddWithValue(DBFields.IsActive, pobjPartner.IsActive);


        //        result = cmd.ExecuteNonQuery();
        //    }
        //    catch (Exception ex)
        //    {
        //        var logger = _loggerFactory.CreateLogger("save partner SP");
        //        logger.LogInformation(ex.Message);
        //    }
        //    finally
        //    {
        //        cmd.Connection.Close();
        //        cmd.Connection = null;
        //    }
        //    return result;
        //}

        //#endregion

        #region Get PartnerEdit
        public static DataSet GetPartnerDetails(int? PartnerId)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetPartnerDetails, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, PartnerId);

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #region Get PartnerContactList
        public static DataSet GetPartnerContactList(int? PartnerId)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetPartnerContactList, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, PartnerId);

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #region Get PartnerProductList
        public static DataSet GetPartnerProductList(int? PartnerId)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetPartnerProductList, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, PartnerId);

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #region Save Product
        public static int InsertProduct(Product pobjProduct)
        {
            int result = 0;
         //   MySqlDataAdapter da;
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertProduct, con);
            XMLCreater obXMLCreater = new XMLCreater();
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.Id, pobjProduct.Id);
                cmd.Parameters.AddWithValue(DBFields.ProductId, pobjProduct.product_id);
                cmd.Parameters.AddWithValue(DBFields.ProductName, pobjProduct.product_name);
                cmd.Parameters.AddWithValue(DBFields.CreatedBy, pobjProduct.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, pobjProduct.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, pobjProduct.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, pobjProduct.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, pobjProduct.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, pobjProduct.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, pobjProduct.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, pobjProduct.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, pobjProduct.IsActive); 
                cmd.Parameters.AddWithValue(DBFields.Variant, pobjProduct.Variant);
                cmd.Parameters.AddWithValue(DBFields.description, pobjProduct.description);
                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save product SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return result;
        }

        #endregion

        #region Get ProductList
        public static DataSet GetProductList()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetProductList, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion
        #region Get ProductList
        public static DataSet GetPartnerProduct(int id)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetPartnerProduct, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.Id, id);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #region Edit Product
        public static DataSet GetProductById(int? ProductId)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetProductEditById, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.Id, ProductId);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion


        #region Get CustomerSupportList
        public static DataSet GetCustomerSupportList()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetCustomerSupportList, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion


        #region Save Customer Support
        public static int InsertCustomerSupport(CustomerSupport pobjCustomerSupport)
        {
            int result = 0;
          //  MySqlDataAdapter da;
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertCustomerSupport, con);
            XMLCreater obXMLCreater = new XMLCreater();
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, pobjCustomerSupport.Id);
                cmd.Parameters.AddWithValue(DBFields.CustomerSupportId, pobjCustomerSupport.CustomerSupportId);
                cmd.Parameters.AddWithValue(DBFields.CompanyId, pobjCustomerSupport.CoampanyId);
                cmd.Parameters.AddWithValue(DBFields.BranchName, pobjCustomerSupport.BranchName);
                cmd.Parameters.AddWithValue(DBFields.Address, pobjCustomerSupport.Address);
                cmd.Parameters.AddWithValue(DBFields.CountryId, pobjCustomerSupport.CountryId);
                cmd.Parameters.AddWithValue(DBFields.StateId, pobjCustomerSupport.StateId);
                cmd.Parameters.AddWithValue(DBFields.CityId, pobjCustomerSupport.CityId);
                cmd.Parameters.AddWithValue(DBFields.Pincode, pobjCustomerSupport.Pincode);
                cmd.Parameters.AddWithValue(DBFields.CustomerSuppEmployeelXml, obXMLCreater.GetCustomerSuppEmployeeXml(pobjCustomerSupport.SupportEmployeeDetailXml));
                cmd.Parameters.AddWithValue(DBFields.CustomerSuppProductXml, obXMLCreater.GetCustomerSuppProductXml(pobjCustomerSupport.SupportProductDetailXml));

                cmd.Parameters.AddWithValue(DBFields.CreatedBy, pobjCustomerSupport.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, pobjCustomerSupport.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, pobjCustomerSupport.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, pobjCustomerSupport.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, pobjCustomerSupport.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, pobjCustomerSupport.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, pobjCustomerSupport.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, pobjCustomerSupport.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, pobjCustomerSupport.IsActive);


                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save CustSupp SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return result;
        }

        #endregion


        // for Customer Support Edit
        #region Get Customer Support Edit
        public static DataSet GetCustomerSupportDetails(int? CustomerSupportId)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetCustomerSupportDetails, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, CustomerSupportId);

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #region Get SupportEmployeeList
        public static DataSet GetCustomerSupportEmployeeList(int? CustomerSupportId)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetCustomerSupportEmployeeList, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, CustomerSupportId);

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #region Get Support ProductList
        public static DataSet GetCustomerSupportProductList(int? CustomerSupportId)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetCustomerSupportProductList, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, CustomerSupportId);

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion
        #region Get PartnerList
        public static DataSet GetPartnerList()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetPartnerList, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #region Company section

        #region Company list
        public static DataSet GetCompanyList()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetCompanyList, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #region Save company
        public static int InsertCompany(Company pobjCompany)
        {
            int result = 0;
            MySqlDataAdapter da;
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertCompany, con);
            XMLCreater obXMLCreater = new XMLCreater();
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, pobjCompany.Id);
                cmd.Parameters.AddWithValue(DBFields.CompanyName, pobjCompany.CompanyName);
                cmd.Parameters.AddWithValue(DBFields.CompanyCode, pobjCompany.CompanyCode);
                cmd.Parameters.AddWithValue(DBFields.Email, pobjCompany.Email);
                cmd.Parameters.AddWithValue(DBFields.Website, pobjCompany.Website);
                cmd.Parameters.AddWithValue(DBFields.ContactNo, pobjCompany.ContactNo);
                cmd.Parameters.AddWithValue(DBFields.Address, pobjCompany.Address);
                cmd.Parameters.AddWithValue(DBFields.CountryId, pobjCompany.CountryId);
                cmd.Parameters.AddWithValue(DBFields.StateId, pobjCompany.StateId);
                cmd.Parameters.AddWithValue(DBFields.CityId, pobjCompany.CityId);
                cmd.Parameters.AddWithValue(DBFields.Pincode, pobjCompany.Pincode);

                cmd.Parameters.AddWithValue(DBFields.TinNo, pobjCompany.TinNo);
                cmd.Parameters.AddWithValue(DBFields.TanNo, pobjCompany.TanNo);
                cmd.Parameters.AddWithValue(DBFields.CinNo, pobjCompany.CinNo);
                cmd.Parameters.AddWithValue(DBFields.GstNo, pobjCompany.GstNo);
                cmd.Parameters.AddWithValue(DBFields.CompanyLogo, pobjCompany.CompanyLogo);

                cmd.Parameters.AddWithValue(DBFields.CreatedBy, pobjCompany.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, pobjCompany.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, pobjCompany.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, pobjCompany.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, pobjCompany.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, pobjCompany.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, pobjCompany.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, pobjCompany.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, pobjCompany.IsActive);


                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save Company SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return result;
        }

        #endregion

        #region Company Edit
        public static DataSet GetCompanyDetailsById(int? CompanyId)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetCompanyDetailsById, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.Id, CompanyId);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #endregion

        #region Branch section
        #region Branch list
        public static DataSet GetBranchList()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetBranchList, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #region Save Branch
        public static int InsertBranch(Branch pobjBranch)
        {
            int result = 0;
            MySqlDataAdapter da;
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertBranch, con);
            XMLCreater obXMLCreater = new XMLCreater();
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, pobjBranch.Id);
                cmd.Parameters.AddWithValue(DBFields.CompanyId, pobjBranch.CompanyId);
                cmd.Parameters.AddWithValue(DBFields.BranchName, pobjBranch.BranchName);
                cmd.Parameters.AddWithValue(DBFields.BranchCode, pobjBranch.BranchCode);
                cmd.Parameters.AddWithValue(DBFields.ContactNo, pobjBranch.ContactNo);
                cmd.Parameters.AddWithValue(DBFields.Address, pobjBranch.Address);
                cmd.Parameters.AddWithValue(DBFields.CountryId, pobjBranch.CountryId);
                cmd.Parameters.AddWithValue(DBFields.StateId, pobjBranch.StateId);
                cmd.Parameters.AddWithValue(DBFields.CityId, pobjBranch.CityId);
                cmd.Parameters.AddWithValue(DBFields.Pincode, pobjBranch.Pincode);

                cmd.Parameters.AddWithValue(DBFields.CreatedBy, pobjBranch.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, pobjBranch.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, pobjBranch.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, pobjBranch.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, pobjBranch.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, pobjBranch.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, pobjBranch.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, pobjBranch.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, pobjBranch.IsActive);


                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save Branch SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return result;
        }

        #endregion

        #region Branch Edit
        public static DataSet GetBranchDetailsById(int? BranchId)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetBranchDetailsById, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.Id, BranchId);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #endregion

        #region Department section
        #region Department list
        public static DataSet GetDepartmentList()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetDepartmentList, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #region Save Department
        public static int InsertDepartment(Department pobjDepartment)
        {
            int result = 0;
            MySqlDataAdapter da;
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertDepartment, con);
            XMLCreater obXMLCreater = new XMLCreater();
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, pobjDepartment.Id);
                cmd.Parameters.AddWithValue(DBFields.CompanyId, pobjDepartment.CompanyId);
                cmd.Parameters.AddWithValue(DBFields.DepartmentName, pobjDepartment.DepartmentName);
                cmd.Parameters.AddWithValue(DBFields.DepartmentCode, pobjDepartment.DepartmentCode);

                cmd.Parameters.AddWithValue(DBFields.CreatedBy, pobjDepartment.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, pobjDepartment.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, pobjDepartment.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, pobjDepartment.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, pobjDepartment.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, pobjDepartment.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, pobjDepartment.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, pobjDepartment.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, pobjDepartment.IsActive);


                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save Dept SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return result;
        }

        #endregion

        #region Department Edit
        public static DataSet GetDepartmentDetailsById(int? DepartmentId)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetDepartmentDetailsById, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.Id, DepartmentId);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #endregion

        #region Designation section
        #region Designation list
        public static DataSet GetDesignationList()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetDesignationList, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #region Save Designation
        public static int InsertDesignation(Designation pobjDesignation)
        {
            int result = 0;
            MySqlDataAdapter da;
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertDesignation, con);
            XMLCreater obXMLCreater = new XMLCreater();
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, pobjDesignation.Id);
                cmd.Parameters.AddWithValue(DBFields.Designation, pobjDesignation.DesignationName);

                cmd.Parameters.AddWithValue(DBFields.CreatedBy, pobjDesignation.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, pobjDesignation.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, pobjDesignation.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, pobjDesignation.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, pobjDesignation.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, pobjDesignation.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, pobjDesignation.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, pobjDesignation.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, pobjDesignation.IsActive);


                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save Dept SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return result;
        }

        #endregion

        #region Designation Edit
        public static DataSet GetDesignationDetailsById(int? DesignationId)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetDesignationDetailsById, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.Id, DesignationId);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #endregion

        #region InsertPartnerContact
        public static DataSet InsertPartnerContact(PartnerContactDetails pobjPartnerContact)
        {
            int result = 0;
            MySqlDataAdapter da;
            DataSet ds =new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertPartnerContact, con);
            XMLCreater obXMLCreater = new XMLCreater();
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, pobjPartnerContact.Id);
                cmd.Parameters.AddWithValue(DBFields.PartnerId, pobjPartnerContact.PartnerId);
                cmd.Parameters.AddWithValue(DBFields.UserName, pobjPartnerContact.UserName);
                cmd.Parameters.AddWithValue(DBFields.Password, pobjPartnerContact.Password);
                cmd.Parameters.AddWithValue(DBFields.FirstName, pobjPartnerContact.FirstName);
                cmd.Parameters.AddWithValue(DBFields.MiddleName, pobjPartnerContact.MiddleName);
                cmd.Parameters.AddWithValue(DBFields.LastName, pobjPartnerContact.LastName);
                cmd.Parameters.AddWithValue(DBFields.ContactNo, pobjPartnerContact.ContactNo);
                cmd.Parameters.AddWithValue(DBFields.Email, pobjPartnerContact.Email);
                cmd.Parameters.AddWithValue(DBFields.DepartmentName, pobjPartnerContact.DepartmentName);
                cmd.Parameters.AddWithValue(DBFields.Designation, pobjPartnerContact.Designation);
                cmd.Parameters.AddWithValue(DBFields.UserId, pobjPartnerContact.UserId);
                cmd.Parameters.AddWithValue(DBFields.CreatedBy, pobjPartnerContact.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, pobjPartnerContact.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, pobjPartnerContact.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, pobjPartnerContact.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, pobjPartnerContact.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, pobjPartnerContact.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, pobjPartnerContact.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, pobjPartnerContact.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, pobjPartnerContact.IsActive);


             //   result = cmd.ExecuteNonQuery();
                  da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save Partner contact SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }

        #endregion
        #region InsertPartnerContact
        public static int InsertCustomerSupportEmployee(CustomerSupportEmployee pobjPartnerContact)
        {
            int result = 0;
            MySqlDataAdapter da;
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertCustomerSupportEmployee, con);
            XMLCreater obXMLCreater = new XMLCreater();
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, pobjPartnerContact.Id);
                cmd.Parameters.AddWithValue(DBFields.cust_support_id, pobjPartnerContact.CustomerSupportId);
                cmd.Parameters.AddWithValue(DBFields.UserName, pobjPartnerContact.UserName);
                cmd.Parameters.AddWithValue(DBFields.Password, pobjPartnerContact.Password);
                cmd.Parameters.AddWithValue(DBFields.FirstName, pobjPartnerContact.FirstName);
                cmd.Parameters.AddWithValue(DBFields.MiddleName, pobjPartnerContact.MiddleName);
                cmd.Parameters.AddWithValue(DBFields.LastName, pobjPartnerContact.LastName);
                cmd.Parameters.AddWithValue(DBFields.ContactNo, pobjPartnerContact.ContactNo);
                cmd.Parameters.AddWithValue(DBFields.Email, pobjPartnerContact.Email);
                cmd.Parameters.AddWithValue(DBFields.DepartmentName, pobjPartnerContact.DepartmentName);
                cmd.Parameters.AddWithValue(DBFields.Designation, pobjPartnerContact.Designation);
                cmd.Parameters.AddWithValue(DBFields.UserId, pobjPartnerContact.UserId);
                cmd.Parameters.AddWithValue(DBFields.CreatedBy, pobjPartnerContact.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, pobjPartnerContact.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, pobjPartnerContact.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, pobjPartnerContact.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, pobjPartnerContact.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, pobjPartnerContact.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, pobjPartnerContact.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, pobjPartnerContact.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, pobjPartnerContact.IsActive);


                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save Partner contact SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return result;
        }

        #endregion

        #region Employee section

        #region Employee list
        public static DataSet GetEmployeeList()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetEmployeeList, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #region Save Employee
        public static int InsertEmployee(Employee pobjEmployee)
        {
            int result = 0;
            MySqlDataAdapter da;
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertEmployee, con);
            XMLCreater obXMLCreater = new XMLCreater();
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, pobjEmployee.Id);
                cmd.Parameters.AddWithValue(DBFields.Title, pobjEmployee.Title);
                cmd.Parameters.AddWithValue(DBFields.FirstName, pobjEmployee.FirstName);
                cmd.Parameters.AddWithValue(DBFields.MiddleName, pobjEmployee.MiddleName);
                cmd.Parameters.AddWithValue(DBFields.LastName, pobjEmployee.LastName);
                cmd.Parameters.AddWithValue(DBFields.UserName, pobjEmployee.UserName);
                cmd.Parameters.AddWithValue(DBFields.Password, pobjEmployee.Password);
                cmd.Parameters.AddWithValue(DBFields.EmployeeCode, pobjEmployee.EmployeeCode);
                cmd.Parameters.AddWithValue(DBFields.CompanyId, pobjEmployee.CompanyId);
                cmd.Parameters.AddWithValue(DBFields.DepartmentId, pobjEmployee.DepartmentId);
                cmd.Parameters.AddWithValue(DBFields.BranchId, pobjEmployee.BranchId);

                cmd.Parameters.AddWithValue(DBFields.DesignationId, pobjEmployee.DesignationId);
                cmd.Parameters.AddWithValue(DBFields.RoleId, pobjEmployee.RoleId);
                cmd.Parameters.AddWithValue(DBFields.FatherName, pobjEmployee.FatherName);
                cmd.Parameters.AddWithValue(DBFields.GenderId, pobjEmployee.GenderId);
                cmd.Parameters.AddWithValue(DBFields.DateOfBirth, pobjEmployee.DateOfBirth);

                cmd.Parameters.AddWithValue(DBFields.MaritalStatus, pobjEmployee.MaritalStatus);
                cmd.Parameters.AddWithValue(DBFields.Email, pobjEmployee.Email);
                cmd.Parameters.AddWithValue(DBFields.ContactNo, pobjEmployee.ContactNo);
                cmd.Parameters.AddWithValue(DBFields.Address, pobjEmployee.Address);
                cmd.Parameters.AddWithValue(DBFields.CountryId, pobjEmployee.CountryId);

                cmd.Parameters.AddWithValue(DBFields.StateId, pobjEmployee.StateId);
                cmd.Parameters.AddWithValue(DBFields.CityId, pobjEmployee.CityId);
                cmd.Parameters.AddWithValue(DBFields.Pincode, pobjEmployee.Pincode);
                cmd.Parameters.AddWithValue(DBFields.Photograph, pobjEmployee.Photograph);
                    cmd.Parameters.AddWithValue(DBFields.UserId, pobjEmployee.UserId);

                cmd.Parameters.AddWithValue(DBFields.CreatedBy, pobjEmployee.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, pobjEmployee.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, pobjEmployee.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, pobjEmployee.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, pobjEmployee.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, pobjEmployee.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, pobjEmployee.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, pobjEmployee.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, pobjEmployee.IsActive);


                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save Employee SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return result;
        }

        #endregion

        #region Employee Edit
        public static DataSet GetEmployeeDetailsById(int? EmployeeId)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetEmployeeDetailsById, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.Id, EmployeeId);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #endregion

        #region Login
        public static DataSet GetUserDetails(UserLogin objuser)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetUserDetails, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.UserName, objuser.UserName);
                cmd.Parameters.AddWithValue(DBFields.Password, objuser.Password);
                cmd.Parameters.AddWithValue(DBFields.LoginDateTime, objuser.LoginDateTime);
                cmd.Parameters.AddWithValue(DBFields.LogOutDateTime, objuser.LogOutDateTime);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, objuser.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, objuser.CreatedMacAddress);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #region save Partner
        public static DataSet InsertPartner(Partner pobjPartner)
        {
            //int result = 0;
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertPartner, con);
            XMLCreater obXMLCreater = new XMLCreater();
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, pobjPartner.Id);
                cmd.Parameters.AddWithValue(DBFields.PartnerIdNumber, pobjPartner.PartnerIdNumber);
                cmd.Parameters.AddWithValue(DBFields.CompanyName, pobjPartner.CompanyName);
                cmd.Parameters.AddWithValue(DBFields.Address, pobjPartner.Address);
                cmd.Parameters.AddWithValue(DBFields.CountryId, pobjPartner.CountryId);
                cmd.Parameters.AddWithValue(DBFields.StateId, pobjPartner.StateId);
                cmd.Parameters.AddWithValue(DBFields.CityId, pobjPartner.CityId);
                cmd.Parameters.AddWithValue(DBFields.Pincode, pobjPartner.Pincode);
                cmd.Parameters.AddWithValue(DBFields.PartnerContactDetailXml, obXMLCreater.GetPartnerContactXml(pobjPartner.PartnerContactDetailXml));
                cmd.Parameters.AddWithValue(DBFields.PartnerProductDetailXml, obXMLCreater.GetPartnerProductXml(pobjPartner.PartnerProductDetailXml));

                cmd.Parameters.AddWithValue(DBFields.CreatedBy, pobjPartner.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, pobjPartner.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, pobjPartner.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, pobjPartner.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, pobjPartner.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, pobjPartner.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, pobjPartner.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, pobjPartner.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, pobjPartner.IsActive);

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
                //result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save partner SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            // return result;
            return ds;
        }

        #endregion
        //#region Login
        //public static DataSet GetUserDetails(UserLogin objuser)
        //{
        //    DataSet ds = new DataSet();
        //    MySqlConnection con = GetConnection();
        //    MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetUserDetails, con);
        //    try
        //    {
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Connection.Open();
        //        cmd.Parameters.AddWithValue(DBFields.UserName, objuser.UserName);
        //        cmd.Parameters.AddWithValue(DBFields.Password, objuser.Password);
        //        MySqlDataAdapter da = new MySqlDataAdapter(cmd);
        //        da.Fill(ds);
        //    }
        //    catch (Exception ex)
        //    {
        //        var logger = _loggerFactory.CreateLogger("LoginCategory");
        //        logger.LogInformation(ex.Message);

        //    }
        //    finally
        //    {
        //        cmd.Connection.Close();
        //        cmd.Connection = null;
        //    }
        //    return ds;
        //}
        //#endregion

        #region Role section
        #region Role list
        public static DataSet GetRoleList()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetRoleList, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #region Save Role
        public static int InsertRole(Role pobjRole)
        {
            int result = 0;
            MySqlDataAdapter da;
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertRole, con);
            XMLCreater obXMLCreater = new XMLCreater();
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, pobjRole.Id);
                cmd.Parameters.AddWithValue(DBFields.RoleName, pobjRole.RoleName);
                cmd.Parameters.AddWithValue(DBFields.RoleDescription, pobjRole.RoleDescription);

                cmd.Parameters.AddWithValue(DBFields.CreatedBy, pobjRole.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, pobjRole.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, pobjRole.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, pobjRole.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, pobjRole.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, pobjRole.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, pobjRole.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, pobjRole.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, pobjRole.IsActive);


                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save Role SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return result;
        }

        #endregion

        #region Role Edit
        public static DataSet GetRoleDetailsById(int? RoleId)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetRoleDetailsById, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.Id, RoleId);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #endregion

        #region Save ProductVariant
        public static int InsertProductVariant(ProductVariant pobjProduct)
        {
            int result = 0;
            //   MySqlDataAdapter da;
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertProductVariant, con);
            XMLCreater obXMLCreater = new XMLCreater();
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, pobjProduct.Id);
                cmd.Parameters.AddWithValue(DBFields.ProductId, pobjProduct.product_code);
                cmd.Parameters.AddWithValue(DBFields.ProductName, pobjProduct.product_name);
                cmd.Parameters.AddWithValue(DBFields.CreatedBy, pobjProduct.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, pobjProduct.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, pobjProduct.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, pobjProduct.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, pobjProduct.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, pobjProduct.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, pobjProduct.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, pobjProduct.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, pobjProduct.IsActive);
                //cmd.Parameters.AddWithValue(DBFields.Variant, pobjProduct.Variant);
                cmd.Parameters.AddWithValue(DBFields.description, pobjProduct.description);
                //cmd.Parameters.AddWithValue(DBFields.ProductTypeId, pobjProduct.product_type_id);
                cmd.Parameters.AddWithValue(DBFields.JsonData, obXMLCreater.GetProductVariantXml(pobjProduct.Variantjson));
                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save product SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return result;
        }

        #region Get ProductVariantList
        public static DataSet GetProductVariantList()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetProductVariantList, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #region Edit Product
        public static DataSet GetProductVariantById(int? Id)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetProductVariantById, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.Id, Id);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #endregion

        #region save Partner
        public static DataSet InsertAuditTb(AuditTb pobjAuditTb)
        {
            //int result = 0;
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertAudit, con);
            XMLCreater obXMLCreater = new XMLCreater();
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.UserId, pobjAuditTb.UserId);
                cmd.Parameters.AddWithValue(DBFields.UsersAuditId, pobjAuditTb.UsersAuditId);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, pobjAuditTb.IpAddress);
                cmd.Parameters.AddWithValue(DBFields.SessionId, pobjAuditTb.SessionId);
                cmd.Parameters.AddWithValue(DBFields.PageAccessed, pobjAuditTb.PageAccessed);
                cmd.Parameters.AddWithValue(DBFields.LoggedInAt, pobjAuditTb.LoggedInAt);
                cmd.Parameters.AddWithValue(DBFields.LoggedOutAt, pobjAuditTb.LoggedOutAt);
                cmd.Parameters.AddWithValue(DBFields.Method, pobjAuditTb.Method);                
                cmd.Parameters.AddWithValue(DBFields.LoginStatus, pobjAuditTb.LoginStatus);
                cmd.Parameters.AddWithValue(DBFields.ControllerName, pobjAuditTb.ControllerName);
                cmd.Parameters.AddWithValue(DBFields.ActionName, pobjAuditTb.ActionName);              
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
                //result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save partner SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            // return result;
            return ds;
        }
        public static DataSet GetProductCustomerCount()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();//new MySqlConnection(GetConnectionString());
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.ProductCustomerCount, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
                // _logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #region InactiveLoginAPIMethod
        public static DataSet InactiveLoginAPIMethod(int PrimaryKey, int type,int UserId)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InactiveLoginAPIMethod, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.Id, PrimaryKey);
                cmd.Parameters.AddWithValue(DBFields.types, type);
                cmd.Parameters.AddWithValue(DBFields.UserId, UserId);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion
    }


}
