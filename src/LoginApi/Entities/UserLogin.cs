﻿using LoginApi.Entities.Baseclasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginApi.Entities
{
    public class UserLogin : BaseClass
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public int UserType { get; set; }
        public DateTime LoginDateTime { get; set; }
        public DateTime LogOutDateTime { get; set; }
        public string Salt { get; set; }

    }
}
