﻿using LoginApi.Entities.Baseclasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginApi.Entities
{

    public class Branch : BaseClass
    {
        public int CompanyId { get; set; }
        public string BranchName { get; set; }
        public string BranchCode { get; set; }
        public string ContactNo { get; set; }
        public string Address { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public int CityId { get; set; }
        public string Pincode { get; set; }

        public string CompanyName { get; set; }

    }
}
