﻿using LoginApi.Entities.Baseclasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginApi.Entities
{
    public class CustomerSupport : BaseClass
    {
        public string CustomerSupportId { get; set; }
        public int CoampanyId { get; set; }
        public string BranchName { get; set; }
        public string Address { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public int CityId { get; set; }
        public string Pincode { get; set; }
        public string SupportEmployeeDetailXml { get; set; }
        public string SupportProductDetailXml { get; set; }
       
    }
}
