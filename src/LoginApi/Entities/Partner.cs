﻿using LoginApi.Entities.Baseclasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginApi.Entities
{
    public class Partner : BaseClass
    {
        //public string PartnerIdNumber { get; set; }
        //public string CompanyName { get; set; }
        //public string Address { get; set; }
        //public int Country { get; set; }
        //public int State { get; set; }
        //public int City { get; set; }
        //public string Pincode { get; set; }
        //public string PartnerContactDetailXml { get; set; }
        //public string PartnerProductDetailXml { get; set; }


        public string PartnerIdNumber { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public int CityId { get; set; }
        public string Pincode { get; set; }
        public string PartnerContactDetailXml { get; set; }
        public string PartnerProductDetailXml { get; set; }


    }
}
