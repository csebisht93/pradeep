﻿using LoginApi.Entities.Baseclasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginApi.Entities
{
    public class ProductVariant : BaseClass
    {
        public string variant_code { get; set; }
        public int product_id { get; set; }
        public string Variant { get; set; }
        public string description { get; set; }

        public string product_name { get; set; }      
        public string product_code { get; set; }
        public int product_type_id { get; set; }
        public string Variantjson { get; set; }
    }

}
