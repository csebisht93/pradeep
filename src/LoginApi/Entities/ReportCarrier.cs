﻿using LoginApi.Entities.Baseclasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginApi.Entities
{
    public class ReportCarrier:BaseClass
    {
         
        public int CustomerCount { get; set; }
        public string Variant_name { get; set; }
        public string Variant_Code { get; set; }
    }
}
