﻿using LoginApi.Entities.Baseclasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginApi.Entities
{
    public class Role:BaseClass
    {
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }

    }
}
