﻿using LoginApi.Entities.Baseclasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginApi.Entities
{
    public class Department : BaseClass
    {
        public int CompanyId { get; set; }
        public string DepartmentName { get; set; }
        public string DepartmentCode { get; set; }

        public string CompanyName { get; set; }


    }
}
