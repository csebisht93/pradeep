﻿using LoginApi.Entities.Baseclasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginApi.Entities
{
    public class Company : BaseClass
    {
        public string CompanyName { get; set; }
        public string CompanyCode { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string ContactNo { get; set; }
        public string Address { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public int CityId { get; set; }
        public string Pincode { get; set; }
        public string TinNo { get; set; }
        public string TanNo { get; set; }
        public string CinNo { get; set; }
        public string GstNo { get; set; }
        public string CompanyLogo { get; set; }


    }
}
