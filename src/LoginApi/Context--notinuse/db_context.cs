﻿using LoginApi.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginApi.Context
{
    public class db_context
    {
        public string ConnectionString { get; set; }

        public db_context(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }

        //public List<User> GetAllUser()
        //{
        //    List<User> list = new List<User>();

        //    using (MySqlConnection conn = GetConnection())
        //    {
        //        conn.Open();
        //        MySqlCommand cmd = new MySqlCommand("select * from cp_country", conn);

        //        using (var reader = cmd.ExecuteReader())
        //        {
        //            while (reader.Read())
        //            {
        //                list.Add(new User()
        //                {
        //                    Id = Convert.ToInt32(reader["Id"]),
        //                    country_name = reader["country_name"].ToString()
                         
        //                });
        //            }
        //        }
        //    }
        //    return list;
        //}
    }
}
