﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Ocelot.Middleware;
using Ocelot.DependencyInjection;

namespace ApiGateway
{
    public class Startup
    {

        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

        }

    

        // This method gets called by the runtime. Use this method to add services to the container.
        public  void ConfigureServices(IServiceCollection services)
        {
            // services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            var identityUrl = Configuration.GetValue<string>("IdentityUrl");
            var authenticationProviderKey = "IdentityApiKey";
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .SetIsOriginAllowed((host) => true)
                    .AllowCredentials());
            });
            services.AddAuthentication()
              .AddJwtBearer(authenticationProviderKey, x =>
              {
                  x.Authority = identityUrl;
                  x.RequireHttpsMetadata = false;
                  x.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters()
                  {
                      ValidAudiences = new[] { "Login-Api", "Upload-Api", "Content-Api"}
                  };
                  x.Events = new Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerEvents()
                  {
                      OnAuthenticationFailed = async ctx =>
                      {
                          int i = 0;
                      },
                      OnTokenValidated = async ctx =>
                      {
                          int i = 0;
                      },

                      OnMessageReceived = async ctx =>
                      {
                          int i = 0;
                      }
                  };
              });
            services.AddOcelot(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public  void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseCors("CorsPolicy");
           // app.UseHttpsRedirection();
            //app.UseMvc();
            app.UseOcelot();
        }
    }
}
