﻿using Log.Adapter;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using UploadEmployeeApi.CommanClasses;
using UploadEmployeeApi.DataAccess.Mapper;
using UploadEmployeeApi.DataAccess.Wrapper;
using UploadEmployeeApi.Entities;
using UploadEmployeeApi.Entities.Baseclasses;
using static UploadEmployeeApi.Controllers.ExcelUploadController;

namespace UploadEmployeeApi.Business
{
    public   class ExcelFacade
    {
        #region Get Country List
        ILoggerFactory _loggerFactory;

        public ServiceResponse SaveExcelUploadEntry(ExcelUploadEntry objExcelUploadEntry)
        {
             
            
             
                return DataWrapper.MapResponse(SPWrapper.SaveExcelUploadEntry(objExcelUploadEntry));
             
           

             
        }
        #endregion
        #region Get Country List
       
        public int SaveUploadEntry(ExcelEntry objExcelEntry)
        {
            //  List<Country> list = new List<Country>();
            int result = 0;
            try
            {
                result = SPWrapper.InsertCustomer(objExcelEntry);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("ExcelCategory");
                logger.LogInformation(ex.Message);

            }

            return result;
        }
        #endregion

        #region Get PdfMemberShipSearch List

        public bool PdfMemberShipSearch(string MemberShipName,string FilePath ,string filename)
        {
            //  List<Country> list = new List<Country>();
            bool result = true;
            try
            {
                result = DataWrapper.PdfMemberShipSearch(SPWrapper.PdfMemberShipSearch(MemberShipName), MemberShipName, FilePath, filename);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("ExcelCategory");
                logger.LogInformation(ex.Message);

            }

            return result;
        }
        #endregion

        #region Customerlist
        public List<Customer> GetCustomerList()
        {
            List<Customer> list = new List<Customer>();
            try
            {
                list = DataWrapper.MapGetCustomerList(SPWrapper.GetCustomerList());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion
        #region Customerlist
        public List<Customer> GetAutoCompleteCustomerList(string SearchName, string SearchMobile, string SearchEmail, string SearchProduct, string SearchMemberShip)
        {
            List<Customer> list = new List<Customer>();
            try
            {
                list = DataWrapper.MapGetCustomerList(SPWrapper.GetAutoCompleteCustomerList(SearchName, SearchMobile, SearchEmail, SearchProduct, SearchMemberShip));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion
 #region Customerlist
        public List<Customer> CustomerAutoCompleteSearch(string Search, int type)
        {
            List<Customer> list = new List<Customer>();
            try
            {
                list = DataWrapper.MapGetCustomerList(SPWrapper.CustomerAutoCompleteSearch(Search, type));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #region Customer Edit
        public List<Customer> GetCustomerDetailsById(int? CustomerId)
        {
            List<Customer> list = new List<Customer>();
            try
            {
                list = DataWrapper.MapGetCustomerDetailsById(SPWrapper.GetCustomerDetailsById(CustomerId));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion
        #region Excel Upload Trackerlist
        public List<ExcelUploadTracker> GetExcelUploadTrackerList(int Id)
        {
            List<ExcelUploadTracker> list = new List<ExcelUploadTracker>(); ;
            try
            {
                list = DataWrapper.MapGetExcelUploadTrackerList(SPWrapper.GetExcelUploadTrackerList(Id));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion
         #region Excel Upload Trackerlist
        public List<ExcelUploadTracker> GetZipUploadTrackerData(int Id)
        {
            List<ExcelUploadTracker> list = new List<ExcelUploadTracker>(); ;
            try
            {
                list = DataWrapper.GetZipUploadTrackerData(SPWrapper.GetZipUploadTrackerData(Id));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #region Customer Edit
        public List<Customer> GetCustomerDetailsByMemberId(string CustomerId)
        {
            List<Customer> list = null;
            try
            {
                list = DataWrapper.MapGetCustomerDetailsById(SPWrapper.GetCustomerDetailsByMemberId(CustomerId));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #region Customer SaveUpdate
        public ServiceResponse InsertUpdateCustomer(Customer pobjCustomer)
        {
            return DataWrapper.MapResponse(SPWrapper.InsertUpdateCustomer(pobjCustomer));
        }
        #endregion
         #region Customer SaveUpdate
        public ServiceResponse InsertUpdateCustomerActivityLog(CustomerActivityLog pobjCustomer)
        {
            return DataWrapper.MapResponse(SPWrapper.InsertCustomerActivityLog(pobjCustomer));
        }
        #endregion

        public ServiceResponse SaveZipUploadEntry(ZipUploadEntry objExcelUploadEntry)
        {
            //  List<Country> list = new List<Country>();



            return DataWrapper.MapResponse(SPWrapper.SaveZipUploadEntry(objExcelUploadEntry));
            
            

             
        }
        
        public List<Customer> GetCustomerSearchByPolicyEmailMobile(string SearchFor)
        {
            List<Customer> list = new List<Customer>();
            try
            {
                list = DataWrapper.SearchByPolicyEmailMobile(SPWrapper.SearchByPolicyEmailMobile(SearchFor));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        public bool SaveCustomerOTP(string Countrycode, string SearchFor, string passcode, int Id, string MembershipNo, string contactno, string Email)
        {
            //List<Customer> list = new List<Customer>();
            try
            {
                return SPWrapper.SaveCustomerOTP(Countrycode, SearchFor, passcode, Id, MembershipNo,contactno,Email);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
                return false;

            }

            //return list;
        }

        public List<Customer> CheckCustomerOTP(Request model)
        {
            List<Customer> list = new List<Customer>();
            try
            {
                list= DataWrapper.BindCustomerDataAfterOTP(SPWrapper.CustomerOTP(model));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
                 

            }

            return list;
        }
        public List<Product> GetCustomerProduct(string SearchFor, string CustomerId,string membershipNo)
        {
            List<Product> list = new List<Product>();
            try
            {
                list = DataWrapper.GetCustomerProduct(SPWrapper.GetCustomerProduct(SearchFor, CustomerId, membershipNo));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }

        public int SaveOTPRequest(OTPEntry oTPEntry,string Comm_ConnectionString)
        {
            //  List<Country> list = new List<Country>();
            int result = 0;
            try
            {
                result = SPWrapper.SaveOTPRequest(oTPEntry, Comm_ConnectionString);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("ExcelCategory");
                logger.LogInformation(ex.Message);

            }

            return result;
        }
        #region Customer Edit
        public Response<OTPEntry> ValidateCustomerOTP(string mobilenumber, string MemberShip,string OTP,string Comm_ConnectionString)
        {
             List<OTPEntry> oTPEntry=new List<OTPEntry>();
            Response<OTPEntry> response = new Response<OTPEntry>(); 
            try
            {
                oTPEntry = DataWrapper.ValidateCustomerOTP(SPWrapper.ValidateCustomerOTP(mobilenumber, MemberShip, OTP, Comm_ConnectionString));
                if (oTPEntry.Count>0)
                {
                    response.Count = oTPEntry.Count;
                    response.ClassObject = oTPEntry.FirstOrDefault();
                    response.ListObject = oTPEntry;
                    if (oTPEntry.FirstOrDefault().OTP == Convert.ToInt32(OTP))
                    {
                        response.Message = "OTP Matched";
                        response.IsSuccess = true;
                        response.Error = "200";
                    }
                    else
                    {
                        response.Message = "OTP not Expired";
                        response.IsSuccess = true;
                        response.Error = "200";
                    }
                }
                else 
                {
                    response.Message = "Please enter correct OTP!";
                    response.IsSuccess = false;
                    response.Error = "401";
                }
            }
            catch (Exception ex)
            {
                // var logger = _loggerFactory.CreateLogger("LoginCategory");
                // logger.LogInformation(ex.Message);
                response.Count = 0;
                response.ClassObject = new OTPEntry();
                response.ListObject = new List<OTPEntry>();
                response.Message = "Something went wrong!";
                response.IsSuccess = false;
                response.Error = "500";

            }
            return response;


        }
        #endregion

        public List<CustomerRequest> SaveCustomerQuery(CustomerRequest model)
        {
            List<CustomerRequest> list = new List<CustomerRequest>();
            try
            {
                list = DataWrapper.SaveCustomerQuery(SPWrapper.SaveCustomerQuery(model));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        public  List<CustomerRequest> GetCustomerQueries(string filter)
        {
            List<CustomerRequest> list = new List<CustomerRequest>();
            try
            {               
                    list = DataWrapper.GetCustomerQueries(SPWrapper.GetCustomerQueries(filter));
            }
            catch (Exception ex)
            {
                throw new NormalException(
                    string.Format(CultureInfo.CurrentCulture, "There was a problem accessing  '{0}'. ({1})", "GetCustomerQueries", ex.Message), ex);


            }

            return list;
        }


        #region GuestQuery
        public ServiceResponse InsertGuestQuery(GuestQuery pobj)
        {
            return DataWrapper.MapResponse(SPWrapper.InsertGuestQuery(pobj));
        }

        #region GuestQuerylist
        public List<GuestQuery> GetGuestQueryList()
        {
            List<GuestQuery> list = new List<GuestQuery>();
            try
            {
                list = DataWrapper.MapGetGuestQueryList(SPWrapper.GetGuestQueryList());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion
        #endregion

        public List<ExcelInfo> GetExcelInfoListByExcelId(int Id)
        {
            List<ExcelInfo> list = new List<ExcelInfo>(); ;
            try
            {
                list = DataWrapper.MapGetExcelInfoListByExcelId(SPWrapper.GetExcelInfoListByExcelId(Id));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            return list;
        }
        #region Report Section 
        public List<CppReport> GetCustomerDocUploadReport(string SearchBy, int type)
        {
            List<CppReport> list = new List<CppReport>();
            try
            {
                list = DataWrapper.MapGetCustomerDocUploadReport(SPWrapper.GetCustomerDocUploadReport(SearchBy, type));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("ReportBusiness");
                logger.LogInformation(ex.Message);

            }

            return list;
        }

        public List<CppReport> GetCustomeEmailReport(string SearchBy, int type, string CommConnectionString)
        {
            List<CppReport> list = new List<CppReport>(); 
            try
            {
                list = DataWrapper.MapGetCustomeEmailReport(SPWrapper.GetCustomeEmailReport(SearchBy, type, CommConnectionString));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("ReportBusiness");
                logger.LogInformation(ex.Message);

            }

            return list;
        } 
        public List<CppReport> GetCustomeSMSReport(string SearchBy, int type, string CommConnectionString)
        {
            List<CppReport> list = new List<CppReport>(); 
            try
            {
                list = DataWrapper.MapGetCustomeSMSReport(SPWrapper.GetCustomeSMSReport(SearchBy, type, CommConnectionString));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("ReportBusiness");
                logger.LogInformation(ex.Message);

            }

            return list;
        }



        #endregion
    }
}
