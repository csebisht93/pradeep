﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UploadEmployeeApi.Entities.BaseClasses;

namespace UploadEmployeeApi.Entities
{
    public class ExcelUploadEntry:BaseClass
    {
        public int UploadId { get; set; }
        public DateTime UploadTime { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileStatus { get; set; }
        public int UploadBy { get; set; }
        public int ProductId { get; set; }  
        public string RelativeFilePath { get; set; }
    }
    public class ExcelUploadTracker : BaseClass
    {

        public string UploadId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileStatus { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string Username { get; set; }
    }
    public class ZipUploadEntry : BaseClass
    {
        public int UploadId { get; set; }
        public DateTime UploadTime { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileStatus { get; set; }
        public int UploadBy { get; set; }
        public int ProductId { get; set; }
        public string RelativeFilePath { get; set; }
    }
    public class ExcelEntry : BaseClass
    {
        public string dob { get; set; }
        public string Name { get; set; }
        public string contactno { get; set; }
        public string Email { get; set; }
        public string membershipno { get; set; }
        public string Product { get; set; }
        public string address { get; set; }
        public string title { get; set; }
        public string alternate_contact_no { get; set; }
        public string alternate_email { get; set; }
        public string pincode { get; set; }
    }
    public class ExcelInfo : BaseClass
    {
        public int ExcelId { get; set; }
        public string Exception { get; set; }
        public string ErrorCode { get; set; }
        public string ExcelLine { get; set; }
        public string ExcelFile { get; set; }
        public Int64 TotalInsert { get; set; }
        public Int64 TotalFail { get; set; }
        public Int64 TotalEntry { get; set; }
        public DateTime ProcessingDate { get; set; }
        public string Filepath { get; set; }
        public string Successfile { get; set; }
        public string FailFile { get; set; }
    }
}
