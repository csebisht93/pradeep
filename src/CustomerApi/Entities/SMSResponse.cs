﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UploadEmployeeApi.Entities
{
    //public class SMSResponse
    //{
    //}
    public class Message
    {
        public string id { get; set; }
        public long recipient { get; set; }
        public string reference { get; set; }
        public string status { get; set; }
    }

    public class Smsdata
    {
        public int count { get; set; }
        public string originator { get; set; }
        public string body { get; set; }
        public object scheduledDateTime { get; set; }
        public int credits { get; set; }
        public int balance { get; set; }
        public List<Message> messages { get; set; }
    }

    public class RootObject
    {
        public int userId { get; set; }
        public string active { get; set; }
        public string message { get; set; }
        public bool status { get; set; }
        public Smsdata smsdata { get; set; }
    }
}
