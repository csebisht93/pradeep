﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UploadEmployeeApi.Entities.BaseClasses;

namespace UploadEmployeeApi.Entities
{
    public class CustomerRequest:BaseClass
    {
        public int RequestId    { get; set; }
        public int RequestType  { get; set; }
        public int CustomerId   { get; set; }
        public string MembershipId { get; set; }
        public string RequestQuery { get; set; }
        public string Additional   { get; set; }
        public int querystatus { get; set; }
        public string CustomerName { get; set; }
        public string text_RequestType { get; set; }
        public string ProductId { get; set; }
        public string AlternateContactNo { get; set; }
        public string Email { get; set; }
        public string contactno { get; set; }
        public string AlternateEmail { get; set; }
        public string product_name { get; set; }
        public Customer CustomersData { get; set; }


    }
    public class CustomerRequestMap : BaseClass
    {
        public int RequestId { get; set; }
        public string RequestQuery { get; set; }
        public string Additional { get; set; }
        public int Querystatus { get; set; }
    }
    public class CustomerActivityLog : BaseClass
    {
        public string membershipNo { get; set; }
        public string CustomerId { get; set; }
        public string Response { get; set; }
        public string RequestOn { get; set; }
        public string Query { get; set; }
        public string RequestFrom { get; set; }
        public string Additional { get; set; }
       
    }
    
}
