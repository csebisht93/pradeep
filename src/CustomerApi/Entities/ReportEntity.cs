﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UploadEmployeeApi.Entities.BaseClasses;

namespace UploadEmployeeApi.Entities
{
    public class ReportEntity : BaseClass
    {
        //ed.id,ed.user_id,ed.`status`,et.description,ed.created_on,ed.is_active,ed.campaign_type,ed.emailto,
        //esc.name as sub_category,ec.name AS category

        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }    
        public string Campaign_type { get; set; }
        public string Emailto { get; set; }
        public string Sub_category { get; set; }
        public string Category { get; set; }
       
    }
    public class CppReport : BaseClass
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductType { get; set; }
        public int RequestType { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string MembershipNo { get; set; }
        public string ChangeType { get; set; }
        public string ContactNo { get; set; }
        public string AlternateContactNo { get; set; }
        public string EmailId { get; set; }
        public string AlternateEmail { get; set; }

        public string DOB { get; set; }
        public string Address { get; set; }
        public string Pincode { get; set; }
        public string ProductVariant { get; set; }
        public string Description { get; set; }
        public string FileName { get; set; }
        public string PolicyNumber { get; set; }
        public string Status { get; set; }
        public string UploadDate { get; set; }
        public string FilePath { get; set; }
        public string Date { get; set; }
        public string FileStatus { get; set; }

        public string UserId { get; set; }
        public string CampaignType { get; set; }
        public string EmailTo { get; set; }
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }



    }
}
