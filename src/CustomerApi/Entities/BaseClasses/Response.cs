﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UploadEmployeeApi.Entities.Baseclasses
{
   
    public class Response<T>
    {
        public Response()
        {
            this.ListObject = new List<T>();
        }
        public string Message { get; set; }
        public string Error { get; set; }
        public List<T> ListObject { get; set; }
        public T ClassObject { get; set; }
        public string Data { get; set; }
        public bool IsSuccess { get; set; }
        public int Count { get; set; }
    }
}
