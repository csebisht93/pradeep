﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UploadEmployeeApi.CommanClasses;
using UploadEmployeeApi.DataAccess.Constants;
using UploadEmployeeApi.DataAccess.Wrapper;
using UploadEmployeeApi.Entities;
using UploadEmployeeApi.Entities.Baseclasses;

namespace UploadEmployeeApi.DataAccess.Mapper
{
    public static class DataWrapper
    {
         
        public static bool PdfMemberShipSearch(bool dataSet, string MemberShipName, string FilePath, string filename)
        {
            var result = false;
            try
            {
               
                if (dataSet)
                {
                    result = SPWrapper.updatePdfMemberShip(MemberShipName, FilePath, filename);
                }
                else
                {
                    result = false;
                    // response.Errcode = Convert.ToInt32(HelperConstants.ErrorCode.InternalError);
                }
            }
            catch(Exception ex) 
            {
                throw new NormalException(
                  string.Format(CultureInfo.CurrentCulture, "There was a problem accessing  '{0}'. ({1})", "PdfMemberShipSearch", ex.Message), ex);

            }
            return result;
        }

        #region Customerlist
        public static List<Customer> MapGetCustomerList(DataSet Ds)
        {
            List<Customer> lobjCustomer = new List<Customer>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCustomer = new List<Customer>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        Customer objectCustomer = new Customer()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id],CultureInfo.CurrentCulture),
                            Title = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Title], CultureInfo.CurrentCulture),
                            Name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Name], CultureInfo.CurrentCulture),
                            MembershipNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.MembershipNo], CultureInfo.CurrentCulture),
                            ContactNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.contactno], CultureInfo.CurrentCulture),
                            Email = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Email], CultureInfo.CurrentCulture),
                            Address = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.address], CultureInfo.CurrentCulture),

                        };
                        lobjCustomer.Add(objectCustomer);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new NormalException(
                   string.Format(CultureInfo.CurrentCulture, "There was a problem accessing  '{0}'. ({1})", "MapGetCustomerList", ex.Message), ex);


            }
            return lobjCustomer;
        }
        #endregion

        #region Customer edit
        public static List<Customer> MapGetCustomerDetailsById(DataSet Ds)
        {
            List<Customer> lobjCustomer = new List<Customer>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCustomer = new List<Customer>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        Customer objectCustomer = new Customer()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id], CultureInfo.CurrentCulture),
                            Title = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Title], CultureInfo.CurrentCulture),
                            Name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Name], CultureInfo.CurrentCulture),
                            MembershipNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.MembershipNo], CultureInfo.CurrentCulture),
                            ContactNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.contactno], CultureInfo.CurrentCulture),
                            AlternateContactNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.AlternateContactNo], CultureInfo.CurrentCulture),
                            Email = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Email], CultureInfo.CurrentCulture),
                            AlternateEmail = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.AlternateEmail], CultureInfo.CurrentCulture),
                            Address = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.address], CultureInfo.CurrentCulture),
                            Pincode = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.pincode], CultureInfo.CurrentCulture),
                            DateOfBirth = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.DateOfBirth], CultureInfo.CurrentCulture),
                            file_name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.FileName], CultureInfo.CurrentCulture),
                            file_path = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.FilePath], CultureInfo.CurrentCulture),
                            CustomerProductList= DataWrapper.MapGetCustomerVariantDetailsById(SPWrapper.CustomerVariantList(Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id], CultureInfo.CurrentCulture))),
                            CustomerActivityLogList= DataWrapper.MapGetCustomerActivityLogList(SPWrapper.CustomerActivityLogList(Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id], CultureInfo.CurrentCulture),""))
                        };
                        lobjCustomer.Add(objectCustomer);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new NormalException(
                 string.Format(CultureInfo.CurrentCulture, "There was a problem accessing  '{0}'. ({1})", "MapGetCustomerDetailsById", ex.Message), ex);

            }
            return lobjCustomer;
        }
        #endregion
        #region Excel Upload Trackerlist
        public static List<ExcelUploadTracker> MapGetExcelUploadTrackerList(DataSet Ds)
        {
            List<ExcelUploadTracker> lobjExcelUploadTracker = new List<ExcelUploadTracker>(); 
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjExcelUploadTracker = new List<ExcelUploadTracker>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        ExcelUploadTracker objectExcelUploadTracker = new ExcelUploadTracker()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id], CultureInfo.CurrentCulture),
                            UploadId = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.UploadId], CultureInfo.CurrentCulture),
                            FileName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.FileName], CultureInfo.CurrentCulture),
                            FilePath = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.FilePath], CultureInfo.CurrentCulture),
                            FileStatus = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.FileStatus], CultureInfo.CurrentCulture),
                            ProductId= Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.ProductId], CultureInfo.CurrentCulture),
                            ProductName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ProductName], CultureInfo.CurrentCulture),
                            CreatedBy = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CreatedBy], CultureInfo.CurrentCulture),
                            CreatedOn = Convert.ToDateTime(Ds.Tables[0].Rows[i][DBFields.CreatedOn], CultureInfo.CurrentCulture),
                            CreatedIP = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.CreatedIP], CultureInfo.CurrentCulture),
                            Username = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.username], CultureInfo.CurrentCulture),
                        };
                        lobjExcelUploadTracker.Add(objectExcelUploadTracker);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new NormalException(
               string.Format(CultureInfo.CurrentCulture, "There was a problem accessing  '{0}'. ({1})", "MapGetExcelUploadTrackerList", ex.Message), ex);

            }
            return lobjExcelUploadTracker;
        }
        #endregion
          #region Excel Upload Trackerlist
        public static List<ExcelUploadTracker> GetZipUploadTrackerData(DataSet Ds)
        {
            List<ExcelUploadTracker> lobjExcelUploadTracker = new List<ExcelUploadTracker>(); 
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjExcelUploadTracker = new List<ExcelUploadTracker>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        ExcelUploadTracker objectExcelUploadTracker = new ExcelUploadTracker()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id], CultureInfo.CurrentCulture),
                            UploadId = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.UploadId], CultureInfo.CurrentCulture),
                            FileName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.FileName], CultureInfo.CurrentCulture),
                            FilePath = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.FilePath], CultureInfo.CurrentCulture),
                            FileStatus = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.FileStatus], CultureInfo.CurrentCulture),
                            ProductId= Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.ProductId], CultureInfo.CurrentCulture),
                            ProductName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ProductName], CultureInfo.CurrentCulture),
                            CreatedBy = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CreatedBy], CultureInfo.CurrentCulture),
                            CreatedOn = Convert.ToDateTime(Ds.Tables[0].Rows[i][DBFields.CreatedOn], CultureInfo.CurrentCulture),
                            CreatedIP = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.CreatedIP], CultureInfo.CurrentCulture),
                            Username = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.username], CultureInfo.CurrentCulture),
                        };
                        lobjExcelUploadTracker.Add(objectExcelUploadTracker);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new NormalException(
               string.Format(CultureInfo.CurrentCulture, "There was a problem accessing  '{0}'. ({1})", "GetZipUploadTrackerData", ex.Message), ex);

            }
            return lobjExcelUploadTracker;
        }
        #endregion
        public static ServiceResponse MapResponse(int result)
        {
            ServiceResponse response = new ServiceResponse();
            if (result > 0)
            {
                switch (result)
                {
                    case 0:
                        response.Errcode = result;
                        break;
                    default:
                        response.Errcode = 200;
                        break;
                }
                response.Id = result;
            }
            else
            {
                response.Errcode = 500;
                // response.Errcode = Convert.ToInt32(HelperConstants.ErrorCode.InternalError);
            }
            return response;
        }
        #region Customer edit
        public static List<Customer> SearchByPolicyEmailMobile(DataSet Ds)
        {
            List<Customer> lobjCustomer = null;
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCustomer = new List<Customer>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        Customer objectCustomer = new Customer()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id], CultureInfo.CurrentCulture),
                            Title = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Title], CultureInfo.CurrentCulture),
                            Name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Name], CultureInfo.CurrentCulture),
                            MembershipNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.MembershipNo], CultureInfo.CurrentCulture),
                            ContactNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.contactno], CultureInfo.CurrentCulture),
                            AlternateContactNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.AlternateContactNo], CultureInfo.CurrentCulture),
                            Email = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Email], CultureInfo.CurrentCulture),
                            AlternateEmail = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.AlternateEmail], CultureInfo.CurrentCulture),
                            Address = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.address], CultureInfo.CurrentCulture),
                            Pincode = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.pincode], CultureInfo.CurrentCulture),
                            DateOfBirth = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.DateOfBirth], CultureInfo.CurrentCulture),
                            

                        };
                        lobjCustomer.Add(objectCustomer);
                    }
                }
            }
            catch (Exception ex)
            {
                
                throw new NormalException(
             string.Format(CultureInfo.CurrentCulture, "There was a problem accessing  '{0}'. ({1})", "SearchByPolicyEmailMobile", ex.Message), ex);

            }
            return lobjCustomer;
        }
        #endregion
          #region Customer edit
        public static List<Product> GetCustomerProduct(DataSet Ds)
        {
            List<Product> lobjCustomer = new List<Product>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCustomer = new List<Product>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        Product objectCustomer = new Product()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id], CultureInfo.CurrentCulture),
                            product_id = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ProductId], CultureInfo.CurrentCulture),
                            product_name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ProductName], CultureInfo.CurrentCulture),
                            file_name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.FileName], CultureInfo.CurrentCulture),
                            file_path = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.FilePath], CultureInfo.CurrentCulture),
                            Variant = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Variant], CultureInfo.CurrentCulture),
                            description= Convert.ToString(Ds.Tables[0].Rows[i][DBFields.description], CultureInfo.CurrentCulture),
                            MemberShip = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.MembershipNo], CultureInfo.CurrentCulture)
                        };
                        lobjCustomer.Add(objectCustomer);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new NormalException(
             string.Format(CultureInfo.CurrentCulture, "There was a problem accessing  '{0}'. ({1})", "GetCustomerProduct", ex.Message), ex);

            }
            return lobjCustomer;
        }
        #endregion

        #region BindCustomerDataAfterOTP edit
        public static List<Customer> BindCustomerDataAfterOTP(DataSet Ds)
        {
            List<Customer> lobjCustomer = new List<Customer>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCustomer = new List<Customer>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        Customer objectCustomer = new Customer()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id], CultureInfo.CurrentCulture),
                            //Title = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Title]),
                            //Name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Name]),
                            //MembershipNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.MembershipNo]),
                            //ContactNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.contactno]),
                            //AlternateContactNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.AlternateContactNo]),
                            //Email = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Email]),
                            //AlternateEmail = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.AlternateEmail]),
                            //Address = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.address]),
                            //Pincode = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.pincode]),
                            //DateOfBirth = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.DateOfBirth]),


                        };
                        lobjCustomer.Add(objectCustomer);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new NormalException(
            string.Format(CultureInfo.CurrentCulture, "There was a problem accessing  '{0}'. ({1})", "BindCustomerDataAfterOTP", ex.Message), ex);

            }
            return lobjCustomer;
        }
        #endregion

        #region Customer edit
        public static List<OTPEntry> ValidateCustomerOTP(DataSet Ds)
        {
            List<OTPEntry> lobjCustomer = new List<OTPEntry>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCustomer = new List<OTPEntry>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        OTPEntry objectCustomer = new OTPEntry()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id], CultureInfo.CurrentCulture),
                            OTP = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.OTP], CultureInfo.CurrentCulture),                           
                            MemberShipNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.membershipno], CultureInfo.CurrentCulture),
                            MobileNumber = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.mobilenumber], CultureInfo.CurrentCulture),
                        };
                        lobjCustomer.Add(objectCustomer);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new NormalException(
          string.Format(CultureInfo.CurrentCulture, "There was a problem accessing  '{0}'. ({1})", "ValidateCustomerOTP", ex.Message), ex);

            }
            return lobjCustomer;
        }
        #endregion

        #region Customer edit
        public static List<CustomerRequest> SaveCustomerQuery(DataSet Ds)
        {
            List<CustomerRequest> lobjCustomer = new List<CustomerRequest>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCustomer = new List<CustomerRequest>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        CustomerRequest objectCustomer = new CustomerRequest()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id],CultureInfo.CurrentCulture),
                        
                  
                        };
                        lobjCustomer.Add(objectCustomer);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new NormalException(
                   string.Format(CultureInfo.CurrentCulture, "There was a problem accessing  '{0}'. ({1})", "SaveCustomerQuery", ex.Message), ex);

            }
            return lobjCustomer;
        }
        #endregion
     #region Customer edit
        public static List<CustomerRequest> GetCustomerQueries(DataSet Ds)
        {
            List<CustomerRequest> lobjCustomer = new List<CustomerRequest>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCustomer = new List<CustomerRequest>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        CustomerRequest objectCustomer = new CustomerRequest()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id], CultureInfo.CurrentCulture),
                            RequestId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Requestid], CultureInfo.CurrentCulture),
                            RequestQuery = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Requestquery], CultureInfo.CurrentCulture),
                            RequestType = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Requesttype], CultureInfo.CurrentCulture),
                            Additional = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.additional], CultureInfo.CurrentCulture),
                            querystatus = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.querystatus], CultureInfo.CurrentCulture),
                            CreatedBy = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CreatedBy], CultureInfo.CurrentCulture),
                            CreatedOn = Convert.ToDateTime(Ds.Tables[0].Rows[i][DBFields.CreatedOn], CultureInfo.CurrentCulture),
                            CustomerName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Name], CultureInfo.CurrentCulture),
                            MembershipId = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.MembershipNo], CultureInfo.CurrentCulture),
                            CustomerId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CustomerId], CultureInfo.CurrentCulture),
                            text_RequestType = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.tRequesttype], CultureInfo.CurrentCulture),
                            IsActive = Convert.ToBoolean(Ds.Tables[0].Rows[i][DBFields.IsActive], CultureInfo.CurrentCulture),
                            Email = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Email], CultureInfo.CurrentCulture),
                            contactno = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.contactno], CultureInfo.CurrentCulture),
                            AlternateEmail = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.AlternateEmail], CultureInfo.CurrentCulture),
                            AlternateContactNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.AlternateContactNo], CultureInfo.CurrentCulture),
                            product_name= Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ProductName], CultureInfo.CurrentCulture),
                        };
                        lobjCustomer.Add(objectCustomer);
                    }
                }
            }
           
            catch (Exception exp)
            {
                throw new NormalException(
                    string.Format(CultureInfo.CurrentCulture,"There was a problem accessing  '{0}'. ({1})", "GetCustomerQueries", exp.Message), exp);
            }
            return lobjCustomer;
        }
        #endregion
        #region GuestQuerylist
        public static List<GuestQuery> MapGetGuestQueryList(DataSet Ds)
        {
            List<GuestQuery> lobjCustomer = new List<GuestQuery>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCustomer = new List<GuestQuery>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        GuestQuery objectCustomer = new GuestQuery()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id], CultureInfo.CurrentCulture),
                            name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Name], CultureInfo.CurrentCulture),
                            mobile_number = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.mobilenumber], CultureInfo.CurrentCulture),
                            email = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Email], CultureInfo.CurrentCulture),
                            question = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Question], CultureInfo.CurrentCulture),
                            CreatedOn = Convert.ToDateTime(Ds.Tables[0].Rows[i][DBFields.CreatedOn], CultureInfo.CurrentCulture),
                            Ago = GetDateago(Convert.ToDateTime(Ds.Tables[0].Rows[i][DBFields.CreatedOn], CultureInfo.CurrentCulture),DateTime.Now)
                        };
                        lobjCustomer.Add(objectCustomer);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new NormalException(
                   string.Format(CultureInfo.CurrentCulture, "There was a problem accessing  '{0}'. ({1})", "MapGetGuestQueryList", ex.Message), ex);

            }
            return lobjCustomer;
        }
        #endregion

        public static string GetDateago(DateTime CreatedDate,DateTime CurrentDate) 
        {
            string DateAgo = string.Empty;
            DateTime start = CreatedDate.Date;
            DateTime end = DateTime.Now.Date;
            TimeSpan difference = end - start;
            if (difference.Days==0)
            {
                DateAgo = "Today";
            }
            else if(difference.Days == 1)
            {
                DateAgo = "Yesterday";
            }
            else if(difference.Days <=3) 
            {
                DateAgo = Convert.ToString(difference.Days, CultureInfo.CurrentCulture)+" "+"Days ago";

            }else
            {
                DateAgo = CreatedDate.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
            }
            return DateAgo;
        }
        public static List<ExcelInfo> MapGetExcelInfoListByExcelId(DataSet Ds)
        {
            List<ExcelInfo> lobjExcelUploadTracker = new List<ExcelInfo>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjExcelUploadTracker = new List<ExcelInfo>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        ExcelInfo objectExcelUploadTracker = new ExcelInfo()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id], CultureInfo.CurrentCulture),
                            ExcelId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.ExcelId], CultureInfo.CurrentCulture),
                            Exception = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Exception], CultureInfo.CurrentCulture),
                            ErrorCode = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ErrorCode], CultureInfo.CurrentCulture),
                            ExcelLine = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ExcelLine], CultureInfo.CurrentCulture),
                            ExcelFile = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ExcelFile], CultureInfo.CurrentCulture),
                            TotalInsert = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.TotalInsert], CultureInfo.CurrentCulture),
                            TotalFail = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.TotalFail], CultureInfo.CurrentCulture),
                            TotalEntry = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.TotalEntry], CultureInfo.CurrentCulture),
                            CreatedBy = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CreatedBy], CultureInfo.CurrentCulture),
                            CreatedOn = Convert.ToDateTime(Ds.Tables[0].Rows[i][DBFields.CreatedOn], CultureInfo.CurrentCulture),
                            CreatedIP = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.CreatedIP], CultureInfo.CurrentCulture),
                            ProcessingDate = Convert.ToDateTime(Ds.Tables[0].Rows[i][DBFields.ProcessingDate], CultureInfo.CurrentCulture),
                            Filepath = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Filepath], CultureInfo.CurrentCulture),
                            Successfile = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Successfile], CultureInfo.CurrentCulture),
                            FailFile = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.FailFile], CultureInfo.CurrentCulture),
                        };
                        lobjExcelUploadTracker.Add(objectExcelUploadTracker);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new NormalException(
               string.Format(CultureInfo.CurrentCulture, "There was a problem accessing  '{0}'. ({1})", "MapGetExcelUploadTrackerList", ex.Message), ex);

            }
            return lobjExcelUploadTracker;
        }

        #region Customer edit
        public static List<CustomerProduct> MapGetCustomerVariantDetailsById(DataSet Ds)
        {
            List<CustomerProduct> lobjCustomer = null;
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCustomer = new List<CustomerProduct>();
                    List<CustomerVariant> customerVariantlist = new List<CustomerVariant>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        CustomerProduct objectCustomer = new CustomerProduct()
                        {
                            
                            ProductId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Product_id], CultureInfo.CurrentCulture),
                            ProductName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ProductName], CultureInfo.CurrentCulture),
                            ProductCode = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ProductCode], CultureInfo.CurrentCulture),

                          

                        };
                        CustomerVariant customerVariant = new CustomerVariant()
                        {
                            ProductId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Product_id], CultureInfo.CurrentCulture),
                            VariantId= Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.VariantId], CultureInfo.CurrentCulture),
                            VariantCode = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.VariantCode], CultureInfo.CurrentCulture),
                            VariantName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Variant], CultureInfo.CurrentCulture),
                            ProductTypeId = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.type], CultureInfo.CurrentCulture),
                            CreatedOn = Convert.ToDateTime(Ds.Tables[0].Rows[i][DBFields.CreatedOn], CultureInfo.CurrentCulture),
                            CreatedBy = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CreatedBy], CultureInfo.CurrentCulture),

                        };
                        objectCustomer.CustomerVariantList = new List<CustomerVariant>();
                        objectCustomer.CustomerVariantList.Add(customerVariant);
                        lobjCustomer.Add(objectCustomer);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new NormalException(
                 string.Format(CultureInfo.CurrentCulture, "There was a problem accessing  '{0}'. ({1})", "MapGetCustomerDetailsById", ex.Message), ex);

            }
            return lobjCustomer;
        }
        #endregion

        #region Customer edit
        public static List<CustomerActivityLog> MapGetCustomerActivityLogList(DataSet Ds)
        {
            List<CustomerActivityLog> lobjCustomer = new List<CustomerActivityLog>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCustomer = new List<CustomerActivityLog>();
                    List<CustomerActivityLog> customerVariantlist = new List<CustomerActivityLog>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        CustomerActivityLog objectCustomer = new CustomerActivityLog()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id], CultureInfo.CurrentCulture),
                            Query = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Requestquery], CultureInfo.CurrentCulture),                          
                        };                                               
                        lobjCustomer.Add(objectCustomer);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new NormalException(
                 string.Format(CultureInfo.CurrentCulture, "There was a problem accessing  '{0}'. ({1})", "MapGetCustomerDetailsById", ex.Message), ex);

            }
            return lobjCustomer;
        }
        #endregion

        #region Report Section 
        public static List<CppReport> MapGetCustomerDocUploadReport(DataSet Ds)
        {
            List<CppReport> lobjCppReport = new List<CppReport>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCppReport = new List<CppReport>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        CppReport objectCppReport = new CppReport()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            FileName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.FileName]),
                            FilePath = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.FilePath]),
                            ProductName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ProductName]),
                            MembershipNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.MembershipNo]),
                            Date = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Date]),
                            FileStatus = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.FileStatus]),
                            Status = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Status])

                        };
                        lobjCppReport.Add(objectCppReport);
                    }
                }
            }
            catch (Exception ex)
            {
                
            }
            return lobjCppReport;
        }

        public static List<CppReport> MapGetCustomeEmailReport(DataSet Ds)
        {
            List<CppReport> lobjCppReport = new List<CppReport>(); 
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCppReport = new List<CppReport>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        CppReport objectCppReport = new CppReport()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            UserId = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.CustomerId]),
                            Status = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Status]),
                            Description = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.description]),
                            Date = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Date]),
                            IsActive = Convert.ToBoolean(Ds.Tables[0].Rows[i][DBFields.IsActive]),
                            CampaignType = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Campaigntype]),
                            EmailTo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.EmailTo]),
                            CategoryName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Category]),
                            SubCategoryName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.SubCategory]),
                            MembershipNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.MembershipNo])//MapGetCustomerDetailsById(SPWrapper.GetCustomerDetailsById(Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CustomerId]))).FirstOrDefault().MembershipNo


                        };
                        lobjCppReport.Add(objectCppReport);
                    }
                }
            }
            catch (Exception ex)
            {
                
            }
            return lobjCppReport;
        }

          public static List<CppReport> MapGetCustomeSMSReport(DataSet Ds)
        {
            List<CppReport> lobjCppReport = new List<CppReport>(); 
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCppReport = new List<CppReport>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        CppReport objectCppReport = new CppReport()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            UserId = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.CustomerId]),
                            Status = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Status]),
                            Description = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.description]),
                            Date = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Date]),
                            IsActive = Convert.ToBoolean(Ds.Tables[0].Rows[i][DBFields.IsActive]),
                            ContactNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.contactno]),
                            CategoryName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Category]),
                            SubCategoryName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.SubCategory]),
                            MembershipNo = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.MembershipNo])//MapGetCustomerDetailsById(SPWrapper.GetCustomerDetailsById(Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.CustomerId]))).FirstOrDefault().MembershipNo


                        };
                        lobjCppReport.Add(objectCppReport);
                    }
                }
            }
            catch (Exception ex)
            {
                
            }
            return lobjCppReport;
        }


        #endregion
    }
}



 
