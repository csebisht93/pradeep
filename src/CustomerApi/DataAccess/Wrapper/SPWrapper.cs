﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using UploadEmployeeApi.DataAccess.Constants;
using UploadEmployeeApi.Entities;
using static UploadEmployeeApi.Controllers.ExcelUploadController;

namespace UploadEmployeeApi.DataAccess.Wrapper
{
    public  class SPWrapper

    {
        static ILoggerFactory _loggerFactory;
        public static string ConnectionString { get; set; }
       
        public SPWrapper(string connectionString)
        {
            ConnectionString = connectionString;
             
        }
      


        
        static MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }
        //public int SaveExcelUploadEntry() 
        //{
        //    return 0;
        //}
        #region save state
        public static int SaveExcelUploadEntry(ExcelUploadEntry pobExcelUploadEntry)
        {
            int result = 0;
            MySqlDataAdapter da;
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertExcelUploadEntry, con);

            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, pobExcelUploadEntry.Id);
                cmd.Parameters.AddWithValue(DBFields.UploadId, pobExcelUploadEntry.UploadId);
                cmd.Parameters.AddWithValue(DBFields.FileName, pobExcelUploadEntry.FileName);
                cmd.Parameters.AddWithValue(DBFields.FilePath, pobExcelUploadEntry.FilePath);
                cmd.Parameters.AddWithValue(DBFields.FileStatus, pobExcelUploadEntry.FileStatus);
                cmd.Parameters.AddWithValue(DBFields.UploadBy, pobExcelUploadEntry.UploadBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedBy, pobExcelUploadEntry.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, pobExcelUploadEntry.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, pobExcelUploadEntry.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, pobExcelUploadEntry.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, pobExcelUploadEntry.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, pobExcelUploadEntry.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, pobExcelUploadEntry.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, pobExcelUploadEntry.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, pobExcelUploadEntry.IsActive); 
                cmd.Parameters.AddWithValue(DBFields.ProductId, pobExcelUploadEntry.ProductId);
                cmd.Parameters.AddWithValue(DBFields.RelativeFilePath, pobExcelUploadEntry.RelativeFilePath);

                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save state SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return result;
        }

        #endregion

        #region save state
        public static int InsertCustomer(ExcelEntry objExcelEntry)
        {
            int result = 0;
            MySqlDataAdapter da;
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertCustomer, con);

            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, objExcelEntry.Id);
                cmd.Parameters.AddWithValue(DBFields.Title, objExcelEntry.title);
                cmd.Parameters.AddWithValue(DBFields.Name, objExcelEntry.Name);
                cmd.Parameters.AddWithValue(DBFields.DateOfBirth, objExcelEntry.dob);                
                cmd.Parameters.AddWithValue(DBFields.AlternateContactNo, objExcelEntry.alternate_contact_no);
                cmd.Parameters.AddWithValue(DBFields.contactno, objExcelEntry.contactno);
                cmd.Parameters.AddWithValue(DBFields.Email, objExcelEntry.Email);  
                cmd.Parameters.AddWithValue(DBFields.AlternateEmail, objExcelEntry.alternate_email);
                cmd.Parameters.AddWithValue(DBFields.MembershipNo, objExcelEntry.membershipno);
                cmd.Parameters.AddWithValue(DBFields.Product, objExcelEntry.Product);
                cmd.Parameters.AddWithValue(DBFields.address, objExcelEntry.address);  
                cmd.Parameters.AddWithValue(DBFields.pincode, objExcelEntry.pincode);
                cmd.Parameters.AddWithValue(DBFields.CreatedBy, objExcelEntry.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, objExcelEntry.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, objExcelEntry.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, objExcelEntry.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, objExcelEntry.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, objExcelEntry.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, objExcelEntry.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, objExcelEntry.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, objExcelEntry.IsActive);


                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save state SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return result;
        }

        #endregion


        #region save state
        public static bool PdfMemberShipSearch(string MemberShipName)
        {
            int result = 0;
            bool booleans = false;
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.PdfMemberShipSearch, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.MembershipNo, MemberShipName);

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
                booleans = true;
            }
            catch (Exception ex)
            {
                booleans = false;
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return booleans;


           
        }

        #endregion

        #region save state
        public static bool updatePdfMemberShip(string MemberShipName, string FilePath,string FileName)
        {
            int result = 0;
            bool booleans = false;
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.updatePdfMemberShip, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.MembershipNo, MemberShipName);
                cmd.Parameters.AddWithValue(DBFields.FilePath, FilePath);
                cmd.Parameters.AddWithValue(DBFields.FileName, FileName);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
                booleans = true;
            }
            catch (Exception ex)
            {
                booleans = false;
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return booleans;



        }

        #endregion

        #region Customerlist
        public static DataSet GetCustomerList()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetCustomerListForSupport, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion
         #region Customerlist
        public static DataSet GetAutoCompleteCustomerList(string SearchName, string SearchMobile, string SearchEmail, string SearchProduct, string SearchMemberShip)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetCustomerListForSupport, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.Name, SearchName);
                cmd.Parameters.AddWithValue(DBFields.mobilenumber, SearchMobile);
                cmd.Parameters.AddWithValue(DBFields.Email, SearchEmail);
                cmd.Parameters.AddWithValue(DBFields.ProductId, SearchProduct);
                cmd.Parameters.AddWithValue(DBFields.MembershipNo, SearchMemberShip);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion
          #region Customerlist
        public static DataSet CustomerAutoCompleteSearch(string Search, int type)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.CustomerAutoCompleteSearch, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.SearchBy, Search);
                cmd.Parameters.AddWithValue(DBFields.type, type);
              
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion


        #region Customer Edit
        public static DataSet GetCustomerDetailsById(int? CustomerId)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetCustomerDetailsById, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.Id, CustomerId);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion
        #region Customer Edit
        public static DataSet GetCustomerDetailsByMemberId(string CustomerId)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetCustomerDetailsByMemberID, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.MembershipNo, CustomerId);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion
        #region Excel Upload Trackerlist
        public static DataSet GetExcelUploadTrackerList(int Id)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetExcelUploadTrackerList, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;               
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.Id, Id);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion
          #region Excel Upload Trackerlist
        public static DataSet GetZipUploadTrackerData(int Id)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetZipUploadTrackerData, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.Id, Id);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #region SaveUpload Customer
        public static int InsertUpdateCustomer(Customer pobjCustomer)
        {
            int result = 0;
            MySqlDataAdapter da;
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertUpdateCustomer, con);

            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, pobjCustomer.Id);
                cmd.Parameters.AddWithValue(DBFields.Title, pobjCustomer.Title);
                cmd.Parameters.AddWithValue(DBFields.Name, pobjCustomer.Name);
                cmd.Parameters.AddWithValue(DBFields.MembershipNo, pobjCustomer.MembershipNo);
                cmd.Parameters.AddWithValue(DBFields.contactno, pobjCustomer.ContactNo);
                cmd.Parameters.AddWithValue(DBFields.AlternateContactNo, pobjCustomer.AlternateContactNo);
                cmd.Parameters.AddWithValue(DBFields.Email, pobjCustomer.Email);
                cmd.Parameters.AddWithValue(DBFields.AlternateEmail, pobjCustomer.AlternateEmail);
                cmd.Parameters.AddWithValue(DBFields.DateOfBirth, pobjCustomer.DateOfBirth);
                cmd.Parameters.AddWithValue(DBFields.address, pobjCustomer.Address);
                cmd.Parameters.AddWithValue(DBFields.pincode, pobjCustomer.Pincode);
                cmd.Parameters.AddWithValue(DBFields.FilePath, pobjCustomer.file_path);
                cmd.Parameters.AddWithValue(DBFields.FileName, pobjCustomer.file_name);


                cmd.Parameters.AddWithValue(DBFields.CreatedBy, pobjCustomer.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, pobjCustomer.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, pobjCustomer.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, pobjCustomer.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, pobjCustomer.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, pobjCustomer.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, pobjCustomer.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, pobjCustomer.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, pobjCustomer.IsActive);  
                cmd.Parameters.AddWithValue(DBFields.CustomerId, pobjCustomer.CustomerId);


                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save Employee SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return result;
        }

        #endregion
        #region SaveUpload Customer
        public static int InsertCustomerActivityLog(CustomerActivityLog pobjCustomer)
        {
            int result = 0;
            MySqlDataAdapter da;
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertCustomerActivityLog, con);

            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                
                cmd.Parameters.AddWithValue(DBFields.MembershipNo, pobjCustomer.membershipNo);
                cmd.Parameters.AddWithValue(DBFields.CustomerId, pobjCustomer.CustomerId);
                cmd.Parameters.AddWithValue(DBFields.Response, pobjCustomer.Response);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, pobjCustomer.RequestOn);
                cmd.Parameters.AddWithValue(DBFields.RequestFrom, pobjCustomer.RequestFrom);
                cmd.Parameters.AddWithValue(DBFields.additional, pobjCustomer.Additional);
                cmd.Parameters.AddWithValue(DBFields.Requestquery, pobjCustomer.Query);
                //cmd.Parameters.AddWithValue(DBFields.ModifiedIP, pobjCustomer.ModifiedIP);
                //cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, pobjCustomer.CreatedMacAddress);
                //cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, pobjCustomer.ModifiedMacAddress);
                //cmd.Parameters.AddWithValue(DBFields.IsActive, pobjCustomer.IsActive);  



                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save Employee SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return result;
        }

        #endregion

        #region save state
        public static int SaveZipUploadEntry(ZipUploadEntry pobExcelUploadEntry)
        {
            int result = 0;
            MySqlDataAdapter da;
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertZipUploadEntry, con);

            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, pobExcelUploadEntry.Id);
                cmd.Parameters.AddWithValue(DBFields.UploadId, pobExcelUploadEntry.UploadId);
                cmd.Parameters.AddWithValue(DBFields.FileName, pobExcelUploadEntry.FileName);
                cmd.Parameters.AddWithValue(DBFields.FilePath, pobExcelUploadEntry.FilePath);
                cmd.Parameters.AddWithValue(DBFields.FileStatus, pobExcelUploadEntry.FileStatus);
                cmd.Parameters.AddWithValue(DBFields.UploadBy, pobExcelUploadEntry.UploadBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedBy, pobExcelUploadEntry.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, pobExcelUploadEntry.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, pobExcelUploadEntry.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, pobExcelUploadEntry.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, pobExcelUploadEntry.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, pobExcelUploadEntry.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, pobExcelUploadEntry.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, pobExcelUploadEntry.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, pobExcelUploadEntry.IsActive);
                cmd.Parameters.AddWithValue(DBFields.ProductId, pobExcelUploadEntry.ProductId);
                cmd.Parameters.AddWithValue(DBFields.RelativeFilePath, pobExcelUploadEntry.RelativeFilePath);


                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save state SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return result;
        }

        #endregion

        #region Customer Edit
        public static DataSet SearchByPolicyEmailMobile(string SearchFor)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.SearchByPolicyEmailMobile, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.SearchBy, SearchFor);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #region save state
        public static bool SaveCustomerOTP(string Countrycode, string SearchFor, string passcode, int Id,string MembershipNo,string contactno,string Email)
        {
            int result = 0;
            bool booleans = false;
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.SaveCustomerOTP, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.Countrycode, Countrycode);
                cmd.Parameters.AddWithValue(DBFields.SearchBy, SearchFor);
                cmd.Parameters.AddWithValue(DBFields.OTP, passcode);
                cmd.Parameters.AddWithValue(DBFields.CustomerId, Id);
                cmd.Parameters.AddWithValue(DBFields.MembershipNo, MembershipNo);
                cmd.Parameters.AddWithValue(DBFields.Email, Email);
                cmd.Parameters.AddWithValue(DBFields.contactno, contactno);

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
                booleans = true;
            }
            catch (Exception ex)
            {
                booleans = false;
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return booleans;



        }

        #endregion
      #region save state
        public static DataSet CustomerOTP(Request model)
        {           
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.CheckCustomerOTPGetData, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();                             
                cmd.Parameters.AddWithValue(DBFields.OTP, model.OTP_Input);
               // cmd.Parameters.AddWithValue(DBFields.CustomerId, model.CustomerId);
                cmd.Parameters.AddWithValue(DBFields.MembershipNo, model.membershipNo);
              //  cmd.Parameters.AddWithValue(DBFields.Email, model.email);
                cmd.Parameters.AddWithValue(DBFields.contactno, model.contactNo);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
               
            }
            catch (Exception ex)
            {
               
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;



        }

        #endregion
        #region Customer Edit
        public static DataSet GetCustomerProduct(string SearchFor, string CustomerId,string membershipNo)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetCustomerProduct, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.SearchBy, SearchFor);
                cmd.Parameters.AddWithValue(DBFields.CustomerId, CustomerId); 
                cmd.Parameters.AddWithValue(DBFields.MembershipNo, membershipNo);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion


        #region save state
        public static int SaveOTPRequest(OTPEntry pobExcelUploadEntry,string Comm_ConnectionString)
        {
            int result = 0;
            MySqlDataAdapter da;
            MySqlConnection con = new MySqlConnection(Comm_ConnectionString);
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertOTPEntries, con);

            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.Id, pobExcelUploadEntry.Id);
                cmd.Parameters.AddWithValue(DBFields.OTP, pobExcelUploadEntry.OTP);
                cmd.Parameters.AddWithValue(DBFields.mobilenumber, pobExcelUploadEntry.MobileNumber);
                cmd.Parameters.AddWithValue(DBFields.msg, pobExcelUploadEntry.Msg);
                cmd.Parameters.AddWithValue(DBFields.status, pobExcelUploadEntry.Status);
                cmd.Parameters.AddWithValue(DBFields.membershipno, pobExcelUploadEntry.MemberShipNo);
                cmd.Parameters.AddWithValue(DBFields.configid, pobExcelUploadEntry.ConfigId);
                cmd.Parameters.AddWithValue(DBFields.templatecode, pobExcelUploadEntry.TemplateCode);
                cmd.Parameters.AddWithValue(DBFields.templateId, pobExcelUploadEntry.TemplateId);
                cmd.Parameters.AddWithValue(DBFields.OtpType, pobExcelUploadEntry.OtpType);
                cmd.Parameters.AddWithValue(DBFields.CreatedBy, pobExcelUploadEntry.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, pobExcelUploadEntry.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, pobExcelUploadEntry.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, pobExcelUploadEntry.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, pobExcelUploadEntry.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, pobExcelUploadEntry.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, pobExcelUploadEntry.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, pobExcelUploadEntry.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, pobExcelUploadEntry.IsActive);



                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save state SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return result;
        }

        #endregion
        #region Customer Edit
        public static DataSet ValidateCustomerOTP(string mobilenumber, string MemberShip, string OTP,string Comm_ConnectionString)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = new MySqlConnection(Comm_ConnectionString);
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.ValidateCustomerOTP, con);
            try
            {
                DateTime RequestDate = DateTime.Now;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.mobilenumber, mobilenumber);
                cmd.Parameters.AddWithValue(DBFields.membershipno, MemberShip);
                cmd.Parameters.AddWithValue(DBFields.OTP, OTP);      
                cmd.Parameters.AddWithValue(DBFields.RequestDate, RequestDate);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion
        #region Customer Edit
        public static DataSet SaveCustomerQuery(CustomerRequest model)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.SaveCustomerRequest, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.Id, model.Id);
                cmd.Parameters.AddWithValue(DBFields.Requestid, model.RequestId);
                cmd.Parameters.AddWithValue(DBFields.Requesttype, model.RequestType);
                cmd.Parameters.AddWithValue(DBFields.CustomerId, model.CustomerId);
                cmd.Parameters.AddWithValue(DBFields.Requestquery, model.RequestQuery);
                cmd.Parameters.AddWithValue(DBFields.additional, model.Additional);
                cmd.Parameters.AddWithValue(DBFields.MembershipNo, model.MembershipId);
                cmd.Parameters.AddWithValue(DBFields.CreatedBy, model.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, model.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, model.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, model.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, model.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, model.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, model.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, model.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, model.IsActive);
                cmd.Parameters.AddWithValue(DBFields.ProductId, model.ProductId);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion
     #region Customer Edit
        public static DataSet GetCustomerQueries(string filter)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetCustomerQueries, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.SearchBy, filter);       
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
               
            }
            return ds;
        }
        #endregion

        #region GuestQuery
        public static int InsertGuestQuery(GuestQuery obj)
        {
            int result = 0;
            MySqlDataAdapter da;
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertGuestQuery, con);

            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.Id, obj.Id);
                cmd.Parameters.AddWithValue(DBFields.Name, obj.name);
                cmd.Parameters.AddWithValue(DBFields.Email, obj.email);
                cmd.Parameters.AddWithValue(DBFields.mobilenumber, obj.mobile_number);
                cmd.Parameters.AddWithValue(DBFields.Question, obj.question);
                cmd.Parameters.AddWithValue(DBFields.CreatedBy, obj.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, obj.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, obj.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, obj.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, obj.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, obj.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, obj.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, obj.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, obj.IsActive);
                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save state SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return result;
        }

        #region GuestQuerylist
        public static DataSet GetGuestQueryList()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetGuestQueryList, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #endregion

        #region Excel Upload Trackerlist
        public static DataSet GetExcelInfoListByExcelId(int Id)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetExcelInfoListByExcelId, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.ExcelId, Id);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #region Customer Edit
        public static DataSet CustomerVariantList(int? CustomerId)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.CustomerVariantList, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.Id, CustomerId);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion


        #region Customer Edit
        public static DataSet CustomerActivityLogList(int? CustomerId,string Requestquery)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetCustomerActivityLogList, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.Id, CustomerId);
                cmd.Parameters.AddWithValue(DBFields.Requestquery, Requestquery);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #region Report Section 
        public static DataSet GetCustomerDocUploadReport(string SearchBy, int type)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetCustomerDocUploadReport, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.SearchBy, SearchBy);
                cmd.Parameters.AddWithValue(DBFields.Type, type);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("Report Srapper");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }


        public static DataSet GetCustomeEmailReport(string SearchBy, int type, string CommConnectionString)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = new MySqlConnection(CommConnectionString);//GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetCustomeEmailReport, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.SearchBy, SearchBy);
                cmd.Parameters.AddWithValue(DBFields.Type, type);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("Report Srapper");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }

  public static DataSet GetCustomeSMSReport(string SearchBy, int type, string CommConnectionString)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = new MySqlConnection(CommConnectionString);//GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetCustomeSMSReport, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.SearchBy, SearchBy);
                cmd.Parameters.AddWithValue(DBFields.Type, type);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("Report Srapper");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }



        #endregion
    }
}
