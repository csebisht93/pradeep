﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using UploadEmployeeApi.Business;
using UploadEmployeeApi.Entities;
using UploadEmployeeApi.Entities.Baseclasses;

namespace UploadEmployeeApi.Controllers
{
    [Route("Upload-Api/[controller]/[action]")]
    [ApiController]
    public class ReportController : ControllerBase
    {  
        IConfiguration _configuration;
        ILoggerFactory _loggerFactory;
        ExcelFacade objExcelFacade = new ExcelFacade();
        public static string CommConnectionString { get; set; }
        public ReportController(IConfiguration configuration) 
        {
            _configuration = configuration;
            CommConnectionString = _configuration["ConnectionStrings:NewCommunication"];
        }
        [HttpGet]
        public Response<ReportEntity> GetEmailReport() 
        {
            objExcelFacade = new ExcelFacade();
            Response<ReportEntity> response = new Response<ReportEntity>();

            return response;
        }
        #region Report Section
        [HttpGet]
        public List<CppReport> GetCustomerDocUploadReport(string SearchBy, int type)
        {
            List<CppReport> objCppReport = new List<CppReport>();
            try
            {
                objCppReport = objExcelFacade.GetCustomerDocUploadReport(SearchBy, type);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("ReportMaster");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                //   countryList = new List<Country>();
            }
            return objCppReport;
        }

        [HttpGet]
        public List<CppReport> GetCustomeEmailReport(string SearchBy, int type)
        {
            List<CppReport> objCppReport = new List<CppReport>();
            try
            {
                objCppReport = objExcelFacade.GetCustomeEmailReport(SearchBy, type, CommConnectionString);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("ReportMaster");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                //   countryList = new List<Country>();
            }
            return objCppReport;
        }


        #endregion


        [HttpGet]
        public List<CppReport> GetCustomeSMSReport(string SearchBy, int type)
        {
            List<CppReport> objCppReport = new List<CppReport>();
            try
            {
                objCppReport = objExcelFacade.GetCustomeSMSReport(SearchBy, type, CommConnectionString);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("ReportMaster");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                //   countryList = new List<Country>();
            }
            return objCppReport;
        }


    }

}