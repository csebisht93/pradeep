﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using UploadEmployeeApi.Business;
using UploadEmployeeApi.Entities;
using UploadEmployeeApi.Entities.Baseclasses;

namespace UploadEmployeeApi.Controllers
{
    [Route("Upload-Api/[controller]/[action]")]
    [ApiController]
    public class ZipUploadController : ControllerBase
    {
        ExcelFacade objExcelFacade = new ExcelFacade();
        ILoggerFactory _loggerFactory;
        [HttpPost]
        [DisableRequestSizeLimit]
        public ServiceResponse UploadZipFile(ZipUploadEntry objExcelUploadEntry)
        {
           
            return objExcelFacade.SaveZipUploadEntry(objExcelUploadEntry);          
           
        }
        // [HttpPost]
        //[DisableRequestSizeLimit]
        //public async Task  UploadZipFile(int ProductId, string ProductName, int CreatedBy, string ipAddress, string macAddress, DateTime CreatedOn)
        //{
        //    string destFile = "";
        //    var GUID = Guid.NewGuid();
        //    string Product = ProductName.ToUpper();
        //    string Year = DateTime.Now.ToString("yyyy");
        //    string Month = DateTime.Now.ToString("MMMM");
        //    string Date = DateTime.Now.ToString("yyyyMMdd");
        //    //Stream bodyStream = HttpContext.Request.Body;
        //    // string folderName = "Unprocess";
        //    string FileName = Product + "_";
        //    string DatabasePath = Product + "/" + Year + "/" + Month + "/" + Date;
        //    string webRootPath = @"ZipFolder/Pending/"+ DatabasePath;
        //  //  string FileName = "Unprocess_Cpp_";
        //  //  string destinationFile = @"DocZipFolder/ZipUploadOn_" + DateTime.Now.ToString("yyyyMMdd") + @"\Processed"+ @"\"+GUID;
          
        //    StringBuilder sb = new StringBuilder();
        //    ZipUploadEntry objExcelUploadEntry = new ZipUploadEntry();
        //    if (!Directory.Exists(webRootPath))
        //    {
        //        Directory.CreateDirectory(webRootPath);
        //    }
        //    if (Request.HasFormContentType)
        //    {
              

        //        var form = Request.Form;
        //        foreach (var formFile in form.Files)
        //        {
        //            var targetDirectory = Path.Combine(webRootPath);//Path.Combine(_hostingEnvironment.WebRootPath, "uploads");

        //            if (!Directory.Exists(targetDirectory))
        //            {
        //                Directory.CreateDirectory(targetDirectory);
        //            }
        //               var fileName = formFile.FileName;// GetFileName(formFile);
        //            string sFileExtension = Path.GetExtension(formFile.FileName).ToLower();
        //            fileName = FileName + Guid.NewGuid() + sFileExtension; //DateTime.Now.ToString("yyyyMMdd_hhmmss")
        //            var savePath = Path.Combine(targetDirectory, fileName);
        //            using (var fileStream = new FileStream(savePath, FileMode.Create))
        //            {
        //                formFile.CopyTo(fileStream);
        //            }
                   
        //           // var fileName = FileName + DateTime.Now.ToString("yyyyMMdd_hhmmss") + sFileExtension;
        //            objExcelUploadEntry.FileName = fileName;
        //            objExcelUploadEntry.FilePath = DatabasePath;
        //            objExcelUploadEntry.FileStatus = "Pending";
        //            objExcelUploadEntry.ProductId = ProductId;
        //            objExcelUploadEntry.CreatedOn = CreatedOn;
        //            objExcelUploadEntry.CreatedBy = CreatedBy;
        //            objExcelUploadEntry.CreatedIP = ipAddress;
        //            objExcelUploadEntry.CreatedMacAddress = macAddress;
        //            await Task.Delay(objExcelFacade.SaveZipUploadEntry(objExcelUploadEntry));


        //           // ArrayList zippedList = UnZipFile(savePath, GUID.ToString());
        //          //  string MemberShipnumber = string.Empty;
        //           // string filenameWithExtension = string.Empty;
        //            //foreach (string filepath in zippedList)
        //            //{
        //            //    filenameWithExtension = Path.GetFileName(filepath);
        //            //    MemberShipnumber = Path.GetFileNameWithoutExtension(filepath);

        //            //    destFile = System.IO.Path.Combine(destinationFile, filenameWithExtension);
        //               // if (objExcelFacade.PdfMemberShipSearch(MemberShipnumber, destFile, filenameWithExtension)) 
        //               // {
                           
        //            //        System.IO.File.Move(filepath, destFile);
        //              // }
        //            //}
                        
        //        }


        //    }


        //    //return (1,"");// sb.ToString();
        //}



        private ArrayList UnZipFile(string fullpath,string GUID)
        {
            ArrayList pathList = new ArrayList(); //contain the number of Excel file.
            try
            {
                if (System.IO.File.Exists(fullpath))
                {
                    string baseDirectory = Path.GetDirectoryName(fullpath);
                   
                    using (ZipInputStream ZipStream = new ZipInputStream(System.IO.File.OpenRead(fullpath)))
                    {
                        ZipEntry theEntry;
                        while ((theEntry = ZipStream.GetNextEntry()) != null)
                        {
                            if (theEntry.IsFile)
                            {
                                if (theEntry.Name != "")
                                {
                                    string strNewFile = @"" + baseDirectory + @"\" + GUID + @"\" + theEntry.Name;
                                    if (System.IO.File.Exists(strNewFile))
                                    {
                                        //continue;
                                    }

                                    using (FileStream streamWriter = System.IO.File.Create(strNewFile))
                                    {
                                        pathList.Add(strNewFile);
                                        int size = 2048;
                                        byte[] data = new byte[2048];
                                        while (true)
                                        {
                                            size = ZipStream.Read(data, 0, data.Length);
                                            if (size > 0)
                                                streamWriter.Write(data, 0, size);
                                            else
                                                break;
                                        }
                                        streamWriter.Close();
                                    }
                                }
                            }
                            else if (theEntry.IsDirectory)
                            {
                                string strNewDirectory = @"" + baseDirectory + @"\"+ GUID + @"\" + theEntry.Name;
                                if (!Directory.Exists(strNewDirectory))
                                {
                                    Directory.CreateDirectory(strNewDirectory);
                                }
                            }
                        }
                        ZipStream.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return pathList;
        }

        #region Excel Upload Trackerlist
        [HttpGet]
        public List<ExcelUploadTracker> GetZipUploadTrackerData(int Id)
        {
            List<ExcelUploadTracker> ExcelUploadTrackerList = new List<ExcelUploadTracker>();
            try
            {
                ExcelUploadTrackerList = objExcelFacade.GetZipUploadTrackerData(Id);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                //   countryList = new List<Country>();
            }
            return ExcelUploadTrackerList;
        }
        #endregion
    }
}