﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Log.Adapter;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UploadEmployeeApi.Business;
using UploadEmployeeApi.Entities;
using UploadEmployeeApi.Entities.Baseclasses;

namespace UploadEmployeeApi.Controllers
{
    [Route("[controller]/[action]")]
 
    public class WelcomeCustomerController : Controller
 
    {
        ExcelFacade objExcelFacade = new ExcelFacade();
        OTPEntry oTPEntry = new OTPEntry();

 
        public static string APISMS { get; set; }

 
        public static string CommConnectionString { get; set; }
 
        IConfiguration _configuration;

       public WelcomeCustomerController(IConfiguration configuration)
 
        {
            _configuration = configuration;
            CommConnectionString = _configuration["ConnectionStrings:Communication"];
            APISMS = _configuration["ConnectionStrings:APISMS"];
        }
        [HttpGet]
        public ActionResult WelcomeCustomer(string MemberShip)
        {
            HttpContext.Session.SetString("policyId", MemberShip);
            return RedirectToAction("RedirectMethod", "WelcomeCustomer");

        }

        [HttpGet]
        //Convert.ToString(ConfigurationManager.AppSettings["APISMS"]);
        public IActionResult RedirectMethod()
        {
            string MemberShip = HttpContext.Session.GetString("policyId");
            List<Customer> objCustomerList = new List<Customer>();
            Customer objCustomer = new Customer();
            oTPEntry = new OTPEntry();
            if (!string.IsNullOrEmpty(MemberShip) && MemberShip != null)
            {
                var ORGMemberShip = Decrypt(MemberShip);
                objCustomer = objExcelFacade.GetCustomerDetailsByMemberId(ORGMemberShip).FirstOrDefault();
                objCustomer.MembershipNo = MemberShip;
                if (objCustomer != null)
                {
                    GenerateOTP(MemberShip).ConfigureAwait(true);
                    HttpContext.Session.Remove("policyId");
                    return View("WelcomeCustomer", objCustomer);
                }
                else
                {
                    return View("FileNotfound", objCustomer);
                }
            }
            else { return View("FileNotfound", objCustomer); }

        }
        [HttpPost]
        public async Task<string> GenerateOTP(string MemberShip)
        {
            Customer objCustomer = new Customer();
            string jsonInput = string.Empty;
            using (var client = new HttpClient())
            {
                try
                {
                    oTPEntry = new OTPEntry();
                    var ORGMemberShip = Decrypt(MemberShip);
                    objCustomer = objExcelFacade.GetCustomerDetailsByMemberId(ORGMemberShip).FirstOrDefault();
                    if (objCustomer != null)
                    {
                        objCustomer.MembershipNo = MemberShip;
                        oTPEntry.MemberShipNo = ORGMemberShip;
                        oTPEntry.MobileNumber = objCustomer.ContactNo;
                        oTPEntry.OTP = GenerateRandomNo();
                        oTPEntry.Status = false;
                        oTPEntry.CreatedBy = 1; //Convert.ToInt64(Session["UserId"]);
                        oTPEntry.CreatedIP = Request.Path;
                        oTPEntry.CreatedMacAddress = Request.Path;
                        oTPEntry.CreatedOn = DateTime.Now;
                        var msg = "Your one time password(OTP) to login to Download File is " + oTPEntry.OTP + ". Do not share this OTP with anyone for security reasons. \n\nRegards \nCPP Group";
                        var postData = "?code=" + 91;
                        postData += "&number=" + objCustomer.ContactNo;
                        postData += "&body=" + msg;
                        oTPEntry.Msg = msg;
                        objExcelFacade.SaveOTPRequest(oTPEntry, CommConnectionString);
                        var response = await client.GetAsync(APISMS + postData).ConfigureAwait(true);
                        jsonInput = response.Content.ReadAsStringAsync().Result;
                        if (!string.IsNullOrEmpty(jsonInput))
                        {
                            jsonInput = "200";
                        }
                        else { jsonInput = "500"; }
                    }

                    return jsonInput;
                }
                catch (Exception ex)
                {
                    // WriteToFiles(JsonConvert.SerializeObject(ex));
                    RootObject sMSResponse = new RootObject();
                    LoggingAdapter.WriteLog(ex.Message + " " + JsonConvert.SerializeObject(sMSResponse));
                    return jsonInput = "500";
                }

            }

            //Random r = new Random();
            //int num = r.Next();
            //return num.ToString();
        }
        public static int GenerateRandomNo()
        {
            int _min = 1000;
            int _max = 9999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max);
        }
        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            if (!string.IsNullOrEmpty(cipherText) && cipherText != null)
            {
                cipherText = cipherText.Replace(" ", "+", StringComparison.OrdinalIgnoreCase);
                byte[] cipherBytes = Convert.FromBase64String(cipherText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] {
                       0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76
                       });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }
                        cipherText = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }
                return cipherText;
            }
            else { return ""; }
        }

        delegate Task<IActionResult> X(Customer a);
        [HttpPost]
        public async Task<IActionResult> RedirectMethod(Customer objCustomerer)
        {
            List<Customer> objCustomerList = new List<Customer>();
            Customer objCustomer = new Customer();
            Response<OTPEntry> response = new Response<OTPEntry>();
            if (objCustomerer.MembershipNo == null)
            {
                ViewBag.msg = "Something went Wrong";
                return View("WelcomeCustomer");
            }
            var ORGMemberShip = Decrypt(objCustomerer.MembershipNo);
            objCustomer = objExcelFacade.GetCustomerDetailsByMemberId(ORGMemberShip).FirstOrDefault();
            response = objExcelFacade.ValidateCustomerOTP(objCustomer.ContactNo, objCustomer.MembershipNo, objCustomerer.OTP, CommConnectionString);
            if (response.IsSuccess)
            {
                try
                {
                    if (!string.IsNullOrEmpty(objCustomer.file_path))
                    {
                        var path = Path.Combine(objCustomer.file_path);
                        var memory = new MemoryStream();
                        using (var stream = new FileStream(path, FileMode.Open))
                        {
                            await stream.CopyToAsync(memory);
                        }
                        memory.Position = 0;
                        ViewBag.msg = response.Message;
                        // return View("WelcomeCustomer");
                        return File(memory, GetContentType(path), objCustomerer.MembershipNo + "." + Path.GetExtension(path));//Path.GetFileName(path)
                    }
                    else
                    {
                        ViewBag.msg = response.Message;
                        return View("WelcomeCustomer");
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.msg = response.Message;
                    LoggingAdapter.WriteLog(ex.Message + " " + JsonConvert.SerializeObject(response));
                    return View("WelcomeCustomer");
                }
            }
            else
            {
                ViewBag.msg = response.Message;
                return View("WelcomeCustomer");
            }


        }
        async Task<IActionResult> FileDownload(Customer objCustomer)
        {
            var path = Path.Combine(objCustomer.file_path);
            // Directory.GetCurrentDirectory(),
            //   "wwwroot", filename);

            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;

            return File(memory, GetContentType(path), objCustomer.MembershipNo + "." + Path.GetExtension(path));


            //using (FileStream fs = System.IO.File.OpenRead(path))
            //{
            //    int length = (int)fs.Length;
            //    byte[] buffer;

            //    using (BinaryReader br = new BinaryReader(fs))
            //    {
            //        buffer = br.ReadBytes(length);
            //    }

            //    Response.Clear();
            //    Response.Buffer = true;
            //    Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", Path.GetFileName(path)));
            //    Response.ContentType = "application/" + Path.GetExtension(path).Substring(1);
            //    Response.BinaryWrite(buffer);
            //    Response.End();
            //}
        }
        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        private static Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformats  officedocument.spreadsheetml.sheet"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }
    }
}