﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Log.Adapter;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using UploadEmployeeApi.Business;
using UploadEmployeeApi.CommanClasses;
using UploadEmployeeApi.Entities;
using UploadEmployeeApi.Entities.Baseclasses;

namespace UploadEmployeeApi.Controllers
{
    [Route("Upload-Api/[controller]/[action]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        ExcelFacade objExcelFacade = new ExcelFacade();
        ILoggerFactory _loggerFactory;
        #region Customerlist
        [HttpGet]
         
        public List<Customer> GetCustomerList()
        {
            List<Customer> objCustomerList = new List<Customer>();
            try
            {
                objCustomerList = objExcelFacade.GetCustomerList();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return objCustomerList;
        }
        #endregion
         #region Customerlist
        [HttpGet]
        public List<Customer> GetAutoCompleteCustomerList(string SearchName, string SearchMobile, string SearchEmail, string SearchProduct, string SearchMemberShip)
        {
            List<Customer> objCustomerList = new List<Customer>();
            try
            {
                objCustomerList = objExcelFacade.GetAutoCompleteCustomerList(SearchName, SearchMobile, SearchEmail, SearchProduct, SearchMemberShip);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return objCustomerList;
        }
        #endregion

        #region Customerlist
        [HttpGet]
        public List<Customer> CustomerAutoCompleteSearch(string Search, int type)
        {
            List<Customer> objCustomerList = new List<Customer>();
            try
            {
                objCustomerList = objExcelFacade.CustomerAutoCompleteSearch(Search, type);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return objCustomerList;
        }
        #endregion


        #region Customer Edit
        [HttpGet]
        public List<Customer> GetCustomerDetailsById(int? CustomerId)
        {
            List<Customer> objCustomerList = new List<Customer>();
            try
            {
                objCustomerList = objExcelFacade.GetCustomerDetailsById(CustomerId);
                
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return objCustomerList;
        }




        #endregion
        [HttpGet]
        public async Task<IActionResult> Download(string filename)
        {
            List<Customer> objCustomerList = new List<Customer>();
            if (filename == null)
                return Content("filename not present");
            objCustomerList = objExcelFacade.GetCustomerDetailsByMemberId(filename);
            var path = Path.Combine(objCustomerList[0].file_path);
                          // Directory.GetCurrentDirectory(),
                        //   "wwwroot", filename);

            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, GetContentType(path), Path.GetFileName(path));
        }
        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformats  officedocument.spreadsheetml.sheet"},  
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }



        #region InsertUpdate Customer
        [HttpPost]
        public ServiceResponse InsertUpdateCustomer(Customer pobjCustomer)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                response = objExcelFacade.InsertUpdateCustomer(pobjCustomer);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("Insert Customer M");
                logger.LogInformation(ex.Message);
            }
            return response;
        }
        #endregion

        [HttpGet]
        public Response<Customer> GetCustomerVerification(string Countrycode,string SearchFor,string passcode)
        {
            Response<Customer> response = new Response<Customer>();
            List<Customer> CustomerSearchList = new List<Customer>();
            objExcelFacade = new ExcelFacade();
            try
            {
                CustomerSearchList = objExcelFacade.GetCustomerSearchByPolicyEmailMobile(SearchFor);              
                if (CustomerSearchList.Count>0)
                {
                    List<Customer> objCustomerList = new List<Customer>();
                    Customer customer = new Customer();
                    customer = CustomerSearchList.FirstOrDefault();
                    var result = objExcelFacade.SaveCustomerOTP(Countrycode, SearchFor, passcode, customer.Id,customer.MembershipNo, customer.ContactNo, customer.Email);
                    if (result)
                    {
                        response.ClassObject = CustomerSearchList[0];
                        response.Count = CustomerSearchList.Count();
                        response.ListObject = objCustomerList;
                        response.Message = "Match";
                        response.Error = "200";

                    }
                    else
                    {
                        response.ClassObject = CustomerSearchList[0];
                        response.Count = CustomerSearchList.Count();
                        response.ListObject = objCustomerList;
                        response.Message = "Customer not Found";
                        response.Error = "404";
                    }
                   
                  
                }
                else
                {
                    response.ClassObject = new Customer();
                    response.Count = CustomerSearchList.Count();
                    response.ListObject = CustomerSearchList;
                    response.Message = "Data Not Found";
                    response.Error = "404";
                }
             
            }
            catch (Exception ex)
            {
                response.Count = 0;
                response.ListObject = CustomerSearchList;
                response.Message = ex.Message;
                response.Error = "500";
                //throw new NormalException(
                //    string.Format(CultureInfo.CurrentCulture, "There was a problem accessing  '{0}'. ({1})", "GetCustomerVerification", ex.Message), response);
                LoggingAdapter.WriteLog(ex.Message + " " + JsonConvert.SerializeObject(response));
                // var logger = _loggerFactory.CreateLogger("LoginCategory");
                // logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return response;
        }

      

        [HttpPost]
        public Response<Customer> GetCheckCustomerOTP(Request model)
        {
            Response<Customer> response = new Response<Customer>();
            List<Customer> CustomerSearchList = new List<Customer>();
            objExcelFacade = new ExcelFacade();
            try
            {
                CustomerSearchList =    objExcelFacade.GetCustomerSearchByPolicyEmailMobile(model.contactNo);
                if (CustomerSearchList.Count > 0)
                {
                    List<Customer> objCustomerList = new List<Customer>();
                    Customer customer = new Customer();
                    customer = CustomerSearchList.FirstOrDefault();
                    var result = objExcelFacade.CheckCustomerOTP(model);
                    if (result.Count>0)
                    {
                        response.ClassObject = CustomerSearchList[0];
                        response.Count = CustomerSearchList.Count();
                        response.ListObject = objCustomerList;
                        response.Message = "Match";
                        response.Error = "200";
                       

                    }
                    else
                    {
                        response.ClassObject = CustomerSearchList[0];
                        response.Count = CustomerSearchList.Count();
                        response.ListObject = objCustomerList;
                        response.Message = "OTP Not Match";
                        response.Error = "404";
                       
                    }


                }
                else
                {
                    response.ClassObject = new Customer();
                    response.Count = CustomerSearchList.Count();
                    response.ListObject = CustomerSearchList;
                    response.Message = "Data Not Found";
                    response.Error = "404";
                 
                }

            }
            catch (Exception ex)
            {
                response.Count = 0;
                response.ListObject = CustomerSearchList;
                response.Message = ex.Message;
                response.Error = "500";
                //throw new NormalException(
                //    string.Format(CultureInfo.CurrentCulture, "There was a problem accessing  '{0}'. ({1})", "GetCheckCustomerOTP", ex.Message), response);
                LoggingAdapter.WriteLog(ex.Message + " " + JsonConvert.SerializeObject(response));

                // var logger = _loggerFactory.CreateLogger("LoginCategory");
                // logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return response;
        }

        [HttpPost]
        public Response<Product> GetCustomerProduct(Request model)
        {
            Response<Product> response = new Response<Product>();
            List<Product> ProductSearchList = new List<Product>();
            objExcelFacade = new ExcelFacade();
            try
            {
                ProductSearchList = objExcelFacade.GetCustomerProduct(model.contactNo,model.CustomerId,model.membershipNo);
                if (ProductSearchList.Count > 0)
                {
                   
                    Product customer = new Product();
                    customer = ProductSearchList.FirstOrDefault();
                   
                        response.ClassObject = ProductSearchList[0];
                        response.Count = ProductSearchList.Count();
                        response.ListObject = ProductSearchList;
                        response.Message = "Match";
                        response.Error = "200";
                  
                }
                else
                {
                    response.ClassObject = new Product();
                    response.Count = ProductSearchList.Count();
                    response.ListObject = ProductSearchList;
                    response.Message = "Data Not Found";
                    response.Error = "404";
                  
                }

            }
            catch (Exception ex)
            {
                response.Count = 0;
                response.ListObject = ProductSearchList;
                response.Message = ex.Message;
                response.Error = "500";
                //throw new NormalException(
                //    string.Format(CultureInfo.CurrentCulture, "There was a problem accessing  '{0}'. ({1})", "GetCustomerProduct", ex.Message), response);
                LoggingAdapter.WriteLog(ex.Message + " " + JsonConvert.SerializeObject(response));
                // var logger = _loggerFactory.CreateLogger("LoginCategory");
                // logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return response;
        }


        [HttpPost]
        public Response<CustomerRequest> SaveCustomerQuery(CustomerRequest model)
        {
            Response<CustomerRequest> response = new Response<CustomerRequest>();
            List<CustomerRequest> CustomerRequestList = new List<CustomerRequest>();
            objExcelFacade = new ExcelFacade();
            try
            {
                model.CreatedOn = DateTime.Now;
                CustomerRequestList = objExcelFacade.SaveCustomerQuery(model);
                if (CustomerRequestList.Count > 0)
                {
                    CustomerRequest customer = new CustomerRequest();
                    customer = CustomerRequestList.FirstOrDefault();
                    response.ClassObject = CustomerRequestList[0];
                    response.Count = CustomerRequestList.Count();
                    response.ListObject = CustomerRequestList;
                    response.Message = "Match";
                    response.Error = "200";
                  
                }
                else
                {
                    response.ClassObject = new CustomerRequest();
                    response.Count = CustomerRequestList.Count();
                    response.ListObject = CustomerRequestList;
                    response.Message = "Data Not Found";
                    response.Error = "404";
                 
                }

            }
            catch (Exception ex)
            {
                response.Count = 0;
                response.ListObject = CustomerRequestList;
                response.Message = ex.Message;
                response.Error = "500";
                //throw new NormalException(
                //       string.Format(CultureInfo.CurrentCulture, "There was a problem accessing  '{0}'. ({1})", "SaveCustomerQuery", ex.Message), response);
                LoggingAdapter.WriteLog(ex.Message + " " + JsonConvert.SerializeObject(response));
                // var logger = _loggerFactory.CreateLogger("LoginCategory");
                // logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return response;
        }


        [HttpGet]
        public Response<CustomerRequest> GetCustomerQuery(string filter)
        {
            Response<CustomerRequest> response = new Response<CustomerRequest>();
            List<CustomerRequest> CustomerRequestList = new List<CustomerRequest>();
            CustomerRequest model = new CustomerRequest();
            objExcelFacade = new ExcelFacade();
            try
            {
                model.CreatedOn = DateTime.Now;
                CustomerRequestList = objExcelFacade.GetCustomerQueries(filter);
                if (CustomerRequestList.Count > 0)
                {
                    CustomerRequest customer = new CustomerRequest();
                    customer = CustomerRequestList.FirstOrDefault();
                    response.ClassObject = CustomerRequestList[0];
                    response.Count = CustomerRequestList.Count();
                    response.ListObject = CustomerRequestList;
                    response.Message = "Match";
                    response.Error = "200";
                
                }
                else
                {
                    response.ClassObject = new CustomerRequest();
                    response.Count = CustomerRequestList.Count();
                    response.ListObject = CustomerRequestList;
                    response.Message = "Data Not Found";
                    response.Error = "404";
                  
                }

            }
            catch (Exception ex)
            {
                response.Count = 0;
                response.ListObject = CustomerRequestList;
                response.Message = ex.Message;
                response.Error = "500";
                // throw new NormalException(
                //     string.Format(CultureInfo.CurrentCulture, "There was a problem accessing  '{0}'. ({1})", "GetCustomerQuery", ex.Message), response);
                LoggingAdapter.WriteLog(ex.Message + " " + JsonConvert.SerializeObject(response));
            }
            finally
            {

            }
            return response;
        }


        [HttpGet]
        public Response<CustomerRequest> GetCustomerQueryById(string filter ,int Id)
        {
            Response<CustomerRequest> response = new Response<CustomerRequest>();
            IEnumerable<CustomerRequest> CustomerRequestList = new List<CustomerRequest>();
            CustomerRequest model = new CustomerRequest();
            objExcelFacade = new ExcelFacade();
            try
            {
                model.CreatedOn = DateTime.Now;
                CustomerRequestList = objExcelFacade.GetCustomerQueries(filter);
                if (CustomerRequestList.Any<CustomerRequest>())
                {
                    CustomerRequest customer = new CustomerRequest();
                    customer = CustomerRequestList.Where(m => m.Id == Id).FirstOrDefault();
                    response.ClassObject = customer;
                    response.Count = CustomerRequestList.Count();
                    response.ListObject = CustomerRequestList.ToList();
                    response.Message = "Match";
                    response.Error = "200";
                
                }
                else
                {
                    response.ClassObject = new CustomerRequest();
                    response.Count = CustomerRequestList.Count();
                    response.ListObject = CustomerRequestList.ToList();
                    response.Message = "Data Not Found";
                    response.Error = "404";
                 
                }

            }
            catch (Exception ex)
            {
                response.Count = 0;
                response.ListObject = CustomerRequestList.ToList();
                response.Message = ex.Message;
                response.Error = "500";
                //throw new NormalException(
                //  string.Format(CultureInfo.CurrentCulture, "There was a problem accessing  '{0}'. ({1})", "GetCustomerQueryById", ex.Message), response);
                LoggingAdapter.WriteLog(ex.Message + " " + JsonConvert.SerializeObject(response));
            }
            finally
            {

            }
            return response;
        }

        [HttpPost]
        public Response<CustomerRequest> InsertCustomerRequest(CustomerRequest model)
        {
            Response<CustomerRequest> response = new Response<CustomerRequest>();
            List<CustomerRequest> CustomerRequestList = new List<CustomerRequest>();
            objExcelFacade = new ExcelFacade();
            try
            {
                model.CreatedOn = DateTime.Now;
               // var asdasd = objExcelFacade.InsertUpdateCustomer(model.CustomersData);
                CustomerRequestList = objExcelFacade.SaveCustomerQuery(model);
                if (CustomerRequestList.Count > 0)
                {
                    CustomerRequest customer = new CustomerRequest();
                    customer = CustomerRequestList.FirstOrDefault();
                    response.ClassObject = CustomerRequestList[0];
                    response.Count = CustomerRequestList.Count();
                    response.ListObject = CustomerRequestList;
                    response.Message = "Match";
                    response.Error = "200";

                }
                else
                {
                    response.ClassObject = new CustomerRequest();
                    response.Count = CustomerRequestList.Count();
                    response.ListObject = CustomerRequestList;
                    response.Message = "Data Not Found";
                    response.Error = "404";

                }

            }
            catch (Exception ex)
            {
                response.Count = 0;
                response.ListObject = CustomerRequestList;
                response.Message = ex.Message;
                response.Error = "500";
                //throw new NormalException(
                //       string.Format(CultureInfo.CurrentCulture, "There was a problem accessing  '{0}'. ({1})", "SaveCustomerQuery", ex.Message), response);
                LoggingAdapter.WriteLog(ex.Message + " " + JsonConvert.SerializeObject(response));
                // var logger = _loggerFactory.CreateLogger("LoginCategory");
                // logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return response;
        }

        [HttpPost]
        public Response<CustomerRequest> InsertUpdateCustomerRequest(CustomerRequest model)
        {
            Response<CustomerRequest> response = new Response<CustomerRequest>();
            List<CustomerRequest> CustomerRequestList = new List<CustomerRequest>();
            objExcelFacade = new ExcelFacade();
            try
            {
                model.CreatedOn = DateTime.Now;
                var asdasd = objExcelFacade.InsertUpdateCustomer(model.CustomersData);
                CustomerRequestList = objExcelFacade.SaveCustomerQuery(model);
                if (CustomerRequestList.Count > 0)
                {
                    CustomerRequest customer = new CustomerRequest();
                    customer = CustomerRequestList.FirstOrDefault();
                    response.ClassObject = CustomerRequestList[0];
                    response.Count = CustomerRequestList.Count();
                    response.ListObject = CustomerRequestList;
                    response.Message = "Match";
                    response.Error = "200";

                }
                else
                {
                    response.ClassObject = new CustomerRequest();
                    response.Count = CustomerRequestList.Count();
                    response.ListObject = CustomerRequestList;
                    response.Message = "Data Not Found";
                    response.Error = "404";

                }

            }
            catch (Exception ex)
            {
                response.Count = 0;
                response.ListObject = CustomerRequestList;
                response.Message = ex.Message;
                response.Error = "500";
                //throw new NormalException(
                //       string.Format(CultureInfo.CurrentCulture, "There was a problem accessing  '{0}'. ({1})", "SaveCustomerQuery", ex.Message), response);
                LoggingAdapter.WriteLog(ex.Message + " " + JsonConvert.SerializeObject(response));
                // var logger = _loggerFactory.CreateLogger("LoginCategory");
                // logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return response;
        }



        [HttpPost]
        public Response<CustomerActivityLog> InsertCustomerActivityLog(CustomerActivityLog model)
        {
            Response<CustomerActivityLog> response = new Response<CustomerActivityLog>();
       
            List<CustomerActivityLog> CustomerRequestList = new List<CustomerActivityLog>();
            CustomerActivityLog Logobject = JsonConvert.DeserializeObject<CustomerActivityLog>(model.Query);
            Logobject.Query = model.Query;
            objExcelFacade = new ExcelFacade();
            try
            {
                ServiceResponse serviceResponse = objExcelFacade.InsertUpdateCustomerActivityLog(Logobject);
            }
            catch (Exception ex)
            {
                response.Count = 0;
                response.ListObject = CustomerRequestList;
                response.Message = ex.Message;
                response.Error = "500";
                //throw new NormalException(
                //       string.Format(CultureInfo.CurrentCulture, "There was a problem accessing  '{0}'. ({1})", "SaveCustomerQuery", ex.Message), response);
                LoggingAdapter.WriteLog(ex.Message + " " + JsonConvert.SerializeObject(response));
                // var logger = _loggerFactory.CreateLogger("LoginCategory");
                // logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return response;
        }
        

    }

}