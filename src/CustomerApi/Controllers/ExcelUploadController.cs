﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
 
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using UploadEmployeeApi.Business;
using UploadEmployeeApi.CommanClasses;
using UploadEmployeeApi.Entities;
using UploadEmployeeApi.Entities.Baseclasses;

namespace UploadEmployeeApi.Controllers
{
    [Route("Upload-Api/[controller]/[action]")]
    [ApiController]
    public class ExcelUploadController : ControllerBase
    {
        private IHostingEnvironment _hostingEnvironment;
        ExcelFacade excelFacade = new ExcelFacade();
        ILoggerFactory _loggerFactory;
        public ExcelUploadController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }       
        [HttpPost]
        public ServiceResponse UploadCustomerFile(ExcelUploadEntry objExcelUploadEntry)
        {

          return excelFacade.SaveExcelUploadEntry(objExcelUploadEntry);





           
        }                   
       //[HttpPost]
       // public string UploadCustomerFile(int ProductId,string ProductName,int CreatedBy,string ipAddress,string macAddress,DateTime CreatedOn)
       // {

       //     //Stream bodyStream = HttpContext.Request.Body;
       //     string Product = ProductName.ToUpper();
       //     string Year = CreatedOn.ToString("yyyy");
       //     string Month = CreatedOn.ToString("MMMM");
       //     string Date = CreatedOn.ToString("yyyyMMdd");
         
       //     string webRootPath = @"FileSystem/PENDING/"+ Product+"/"+ Year + "/" + Month + "/" + Date;
       //     string DatabasePath = Product + "/" + Year + "/" + Month + "/" + Date;
       //     string FileName = Product+"_";
       //     StringBuilder sb = new StringBuilder();
       //     ExcelUploadEntry objExcelUploadEntry = new ExcelUploadEntry();
          
       //     if (Request.HasFormContentType)
       //     {
       //         var form = Request.Form;
       //         foreach (var formFile in form.Files)
       //         {
       //             var targetDirectory = Path.Combine(webRootPath);//Path.Combine(_hostingEnvironment.WebRootPath, "uploads");

       //             if (!Directory.Exists(targetDirectory))
       //             {
       //                 Directory.CreateDirectory(targetDirectory);
       //             }
       //          //   var fileName = FileName + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xlsx";// GetFileName(formFile);

       //             //var savePath = Path.Combine(targetDirectory, fileName);

       //             //using (var fileStream = new FileStream(savePath, FileMode.Create))
       //             //{
       //             //    formFile.CopyTo(fileStream);
       //             //}
       //             if (formFile.Length > 0)
       //             {
       //                 string sFileExtension = Path.GetExtension(formFile.FileName).ToLower();
       //                 var fileName = FileName + CreatedOn.ToString("yyyyMMdd_hhmmss") + sFileExtension;
       //                 objExcelUploadEntry.FileName = fileName;
       //                 objExcelUploadEntry.FilePath = DatabasePath;
       //                 objExcelUploadEntry.FileStatus = "Pending";
       //                 objExcelUploadEntry.ProductId = ProductId;
       //                 objExcelUploadEntry.CreatedOn = CreatedOn;
       //                 objExcelUploadEntry.CreatedBy = CreatedBy;
       //                 objExcelUploadEntry.CreatedIP = ipAddress;
       //                 objExcelUploadEntry.CreatedMacAddress = macAddress;
       //                 excelFacade.SaveExcelUploadEntry(objExcelUploadEntry);
       //                 ISheet sheet;
       //                 string fullPath = Path.Combine(targetDirectory, fileName);
       //                 using (var stream = new FileStream(fullPath, FileMode.Create))  //FileMode.Open, FileAccess.Read
       //                 {
       //                     formFile.CopyTo(stream);
       //                     stream.Position = 0;
       //                     if (sFileExtension == ".xls")
       //                     {
       //                         HSSFWorkbook hssfwb = new HSSFWorkbook(stream); //This will read the Excel 97-2000 formats  
       //                         sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook  
       //                     }
       //                     else
       //                     {
       //                         XSSFWorkbook hssfwb = new XSSFWorkbook(stream); //This will read 2007 Excel format  
       //                         sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook   
       //                     }
       //                     IRow headerRow = sheet.GetRow(0); //Get Header Row
       //                     int cellCount = headerRow.LastCellNum;
       //                     sb.Append("<table class='table'><tr>");
       //                     for (int j = 0; j < cellCount; j++)
       //                     {
       //                         NPOI.SS.UserModel.ICell cell = headerRow.GetCell(j);
       //                         if (cell == null || string.IsNullOrWhiteSpace(cell.ToString())) continue;
       //                         sb.Append("<th>" + cell.ToString() + "</th>");
       //                     }
       //                     sb.Append("</tr>");
       //                     sb.AppendLine("<tr>");

       //                     for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++) //Read Excel File
       //                     {
       //                         IRow row = sheet.GetRow(i);
       //                         ExcelEntry objExcelEntry = new ExcelEntry();
       //                         if (row == null) continue;
       //                         if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;
       //                         // for (int j = row.FirstCellNum; j < cellCount; j++)
       //                         // {

       //                         //  if (row.GetCell(j) != null)
       //                         // {



       //                         // objExcelEntry.Name = row.GetCell(0).ToString();
       //                         //objExcelEntry.Name = row.GetCell(1).ToString();
       //                         //objExcelEntry.contactno = row.GetCell(2).ToString();
       //                         //objExcelEntry.Email = row.GetCell(3).ToString();
       //                         //objExcelEntry.membershipno = row.GetCell(4).ToString();
       //                         //objExcelEntry.Product = row.GetCell(5).ToString();


       //                         // sb.Append("<td>" + row.GetCell(j).ToString() + "</td>");
       //                         //   }
       //                         // }
       //                            //excelFacade.SaveUploadEntry(objExcelEntry);
       //                         sb.AppendLine("</tr>");
       //                     }
       //                     sb.Append("</table>");
       //                 }
       //             }
       //         }


       //     }


       //     return sb.ToString();
       // }                   
       

        #region Excel Upload Trackerlist
        [HttpGet]
        public List<ExcelUploadTracker> GetExcelUploadTrackerList(int Id)
        {
            List<ExcelUploadTracker> ExcelUploadTrackerList = new List<ExcelUploadTracker>();
            try
            {
                ExcelUploadTrackerList = excelFacade.GetExcelUploadTrackerList(Id);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                //   countryList = new List<Country>();
            }
            return ExcelUploadTrackerList;
        }
        #endregion


        //public ActionResult OnPostImport()
        //{



        //    string folderName = "ExcelFolder";
        //    string webRootPath = @"Uploads/UploadOn"; //_hostingEnvironment.WebRootPath;
        //    string newPath = Path.Combine(webRootPath, folderName);
        //    if (!System.IO.File.Exists(newPath))
        //    {

        //    }

        //    //  StringBuilder sb = new StringBuilder();
        //    if (!Directory.Exists(newPath))
        //    {
        //        Directory.CreateDirectory(newPath);
        //    }


        //    // newPath = newPath + "/Cpptest_1 - Copy.xlsx";
        //    var physicalFile = new FileInfo(newPath + "/Test.xlsx");
        //    //     MockPhysicalFile objMockPhysicalFile = new MockPhysicalFile();
        //    IFormFile file = MockPhysicalFile.AsMockIFormFile(physicalFile);
        //    if (file.Length > 0)
        //    {
        //        string sFileExtension = Path.GetExtension(file.FileName).ToLower();
        //        ISheet sheet;
        //        string fullPath = Path.Combine(newPath, file.FileName);
        //        using (var stream = new FileStream(fullPath, FileMode.Open, FileAccess.Read))
        //        {
        //            file.CopyTo(stream);
        //            stream.Position = 0;
        //            if (sFileExtension == ".xls")
        //            {
        //                HSSFWorkbook hssfwb = new HSSFWorkbook(stream); //This will read the Excel 97-2000 formats  
        //                sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook  
        //            }
        //            else
        //            {
        //                XSSFWorkbook hssfwb = new XSSFWorkbook(stream); //This will read 2007 Excel format  
        //                sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook   
        //            }
        //            IRow headerRow = sheet.GetRow(0); //Get Header Row
        //            int cellCount = headerRow.LastCellNum;
        //            //   sb.Append("<table class='table'><tr>");
        //            for (int j = 0; j < cellCount; j++)
        //            {
        //                NPOI.SS.UserModel.ICell cell = headerRow.GetCell(j);
        //                if (cell == null || string.IsNullOrWhiteSpace(cell.ToString())) continue;
        //                //     sb.Append("<th>" + cell.ToString() + "</th>");
        //            }
        //            //  sb.Append("</tr>");
        //            //   sb.AppendLine("<tr>");
        //            for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++) //Read Excel File
        //            {
        //                IRow row = sheet.GetRow(i);
        //                if (row == null) continue;
        //                if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;
        //                for (int j = row.FirstCellNum; j < cellCount; j++)
        //                {
        //                    if (row.GetCell(j) != null)
        //                    {

        //                    }
        //                    //              sb.Append("<td>" + row.GetCell(j).ToString() + "</td>");
        //                }
        //                //      sb.AppendLine("</tr>");
        //            }
        //            //   sb.Append("</table>");
        //        }
        //    }
        //    return this.Content("PAss");
        //}

        [HttpGet]
        public  ExcelInfo GetExcelInfoByExcelId(int Id) 
        {
           
            List<ExcelInfo> excelInfosList = excelFacade.GetExcelInfoListByExcelId(Id);
            ExcelInfo excelInfo;
            if (excelInfosList != null)
            {
                excelInfo = excelInfosList.FirstOrDefault();
            }
            else {
                excelInfo = new ExcelInfo();
            }
            return excelInfo;
        }

        


        //[HttpPost]
        //public ActionResult ExcelUploadMethod()
        //{
        //    DataSet ds = new DataSet();
        //    if (Request.Files["file"].ContentLength > 0)
        //    {
        //        string fileExtension =System.IO.Path.GetExtension(Request.Files["file"].FileName);

        //        if (fileExtension == ".xls" || fileExtension == ".xlsx")
        //        {
        //            string fileLocation = Server.MapPath("~/Content/") + Request.Files["file"].FileName;
        //            if (System.IO.File.Exists(fileLocation))
        //            {

        //                System.IO.File.Delete(fileLocation);
        //            }
        //            Request.Files["file"].SaveAs(fileLocation);
        //            string excelConnectionString = string.Empty;
        //            excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
        //            fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
        //            //connection String for xls file format.
        //            if (fileExtension == ".xls")
        //            {
        //                excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" +
        //                fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
        //            }
        //            //connection String for xlsx file format.
        //            else if (fileExtension == ".xlsx")
        //            {
        //                excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
        //                fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
        //            }
        //            //Create Connection to Excel work book and add oledb namespace
        //            OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
        //            excelConnection.Open();
        //            DataTable dt = new DataTable();

        //            dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
        //            if (dt == null)
        //            {
        //                return null;
        //            }

        //            String[] excelSheets = new String[dt.Rows.Count];
        //            int t = 0;
        //            //excel data saves in temp file here.
        //            foreach (DataRow row in dt.Rows)
        //            {
        //                excelSheets[t] = row["TABLE_NAME"].ToString();
        //                t++;
        //            }
        //            OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);


        //            string query = string.Format("Select * from [{0}]", excelSheets[0]);
        //            using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
        //            {
        //                dataAdapter.Fill(ds);
        //            }
        //        }
        //        if (fileExtension.ToString().ToLower().Equals(".xml"))
        //        {
        //            string fileLocation = Server.MapPath("~/Content/") + Request.Files["FileUpload"].FileName;
        //            if (System.IO.File.Exists(fileLocation))
        //            {
        //                System.IO.File.Delete(fileLocation);
        //            }

        //            Request.Files["FileUpload"].SaveAs(fileLocation);
        //            XmlTextReader xmlreader = new XmlTextReader(fileLocation);
        //            // DataSet ds = new DataSet();
        //            ds.ReadXml(xmlreader);
        //            xmlreader.Close();
        //        }

        //        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        //        {
        //            string conn = ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString;
        //            SqlConnection con = new SqlConnection(conn);
        //            string query = "Insert into Person(Name,Email,Mobile) Values('" +
        //            ds.Tables[0].Rows[i][0].ToString() + "','" + ds.Tables[0].Rows[i][1].ToString() +
        //            "','" + ds.Tables[0].Rows[i][2].ToString() + "')";
        //            con.Open();
        //            SqlCommand cmd = new SqlCommand(query, con);
        //            cmd.ExecuteNonQuery();
        //            con.Close();
        //        }
        //    }
        //    return View();
        //}
    }
}