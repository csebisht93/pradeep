﻿using Log.Adapter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UploadEmployeeApi.CommanClasses
{
    public class NormalException : Exception
    {
         

        public NormalException(string message) : base(message)
        {
        }

        public NormalException(string message, Exception innerException) : base(message, innerException)
        {
            LoggingAdapter.WriteLog(message+" "+JsonConvert.SerializeObject(innerException));
        }

        public NormalException(string message, object objecter )  
        {
            LoggingAdapter.WriteLog(message + " " + JsonConvert.SerializeObject(objecter));
        }

        public NormalException()
        {
        }
    }

    
}
