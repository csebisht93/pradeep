﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContentManageApi.DataAccess.Constant
{
    public class DBFields
    {
        //-----------Common Base class field ---------------------------
        public static string Id = "id";
        public static string IsActive = "is_active";
        public static string CreatedBy = "created_by";
        public static string CreatedOn = "created_on";
        public static string ModifiedBy = "modified_by";
        public static string ModifiedOn = "modified_on";
        public static string CreatedIP = "ip_address";
        public static string ModifiedIP = "modified_ip";
        public static string CreatedMacAddress = "mac_address";
        public static string ModifiedMacAddress = "modified_mac";
        public static string ErrCode = "ErrCode";

        public static string Content = "content";
        public static string AdditionalField = "additional_field";




    }

    public class StoredProcedures
    {
        public static string GetContactDetails = "sp_GetContactDetails";
        public static string GetHomePageDetails = "sp_GetHomePageDetails";
        public static string GetInfoPageDetails = "sp_GetInfoPageDetails";
        public static string GetPlanPageDetails = "sp_GetPlanPageDetails";
        public static string GetFAQPageDetails = "sp_GetFAQPageDetails";
        public static string GetFooterPageDetails = "sp_GetFooterPageDetails";
        public static string GetHeaderPageDetails = "sp_GetHeaderPageDetails";
    }

}
