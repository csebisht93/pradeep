﻿using ContentManageApi.DataAccess.Constant;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ContentManageApi.DataAccess.Wrapper
{
    public class SPWrapper
    {

        static ILoggerFactory _loggerFactory;
        public static string ConnectionString { get; set; }

        public SPWrapper(string connectionString)
        {
            ConnectionString = connectionString;
        }

        static MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }

        public static DataSet GetContactDetails()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetContactDetails, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();               
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
              
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }

        public static DataSet GetHomePageDetails()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetHomePageDetails, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }

        public static DataSet GetInfoPageDetails()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetInfoPageDetails, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }


        public static DataSet GetPlanPageDetails()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetPlanPageDetails, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }

        public static DataSet GetFAQPageDetails()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetFAQPageDetails, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }

        public static DataSet GetFooterPageDetails()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetFooterPageDetails, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }

        public static DataSet GetHeaderPageDetails()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetHeaderPageDetails, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }




    }
}
