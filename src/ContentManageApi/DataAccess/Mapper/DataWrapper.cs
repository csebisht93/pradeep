﻿using ContentManageApi.DataAccess.Constant;
using ContentManageApi.Entities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ContentManageApi.DataAccess.Mapper
{
    public static class DataWrapper
    {
        static ILoggerFactory _loggerFactory;

        #region GetContactDetails
        public static List<CommonClass> MapGetContactDetails(DataSet Ds)
        {
            List<CommonClass> lobjCountry = new List<CommonClass>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCountry = new List<CommonClass>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        CommonClass objectCountry = new CommonClass()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            Content = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Content]),
                            AdditionalField = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.AdditionalField]),

                        };
                        lobjCountry.Add(objectCountry);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjCountry;
        }
        #endregion


        #region GetHomePageDetails
        public static List<CommonClass> MapGetHomePageDetails(DataSet Ds)
        {
            List<CommonClass> lobjCountry = new List<CommonClass>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCountry = new List<CommonClass>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        CommonClass objectCountry = new CommonClass()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            Content = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Content]),
                            AdditionalField = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.AdditionalField]),

                        };
                        lobjCountry.Add(objectCountry);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjCountry;
        }
        #endregion

        #region GetInfoPageDetails
        public static List<CommonClass> MapGetInfoPageDetails(DataSet Ds)
        {
            List<CommonClass> lobjCountry = new List<CommonClass>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCountry = new List<CommonClass>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        CommonClass objectCountry = new CommonClass()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            Content = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Content]),
                            AdditionalField = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.AdditionalField]),

                        };
                        lobjCountry.Add(objectCountry);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjCountry;
        }
        #endregion


        #region GetPlanPageDetails
        public static List<CommonClass> MapGetPlanPageDetails(DataSet Ds)
        {
            List<CommonClass> lobjCountry = new List<CommonClass>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCountry = new List<CommonClass>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        CommonClass objectCountry = new CommonClass()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            Content = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Content]),
                            AdditionalField = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.AdditionalField]),

                        };
                        lobjCountry.Add(objectCountry);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjCountry;
        }
        #endregion


        #region GetFAQPageDetails
        public static List<CommonClass> MapGetFAQPageDetails(DataSet Ds)
        {
            List<CommonClass> lobjCountry = new List<CommonClass>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCountry = new List<CommonClass>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        CommonClass objectCountry = new CommonClass()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            Content = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Content]),
                            AdditionalField = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.AdditionalField]),

                        };
                        lobjCountry.Add(objectCountry);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjCountry;
        }
        #endregion

        #region GetFooterPageDetails
        public static List<CommonClass> MapGetFooterPageDetails(DataSet Ds)
        {
            List<CommonClass> lobjCountry = new List<CommonClass>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCountry = new List<CommonClass>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        CommonClass objectCountry = new CommonClass()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            Content = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Content]),
                            AdditionalField = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.AdditionalField]),

                        };
                        lobjCountry.Add(objectCountry);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjCountry;
        }
        #endregion

        #region GetHeaderPageDetails
        public static List<CommonClass> MapGetHeaderPageDetails(DataSet Ds)
        {
            List<CommonClass> lobjCountry = new List<CommonClass>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjCountry = new List<CommonClass>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        CommonClass objectCountry = new CommonClass()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            Content = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Content]),
                            AdditionalField = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.AdditionalField]),

                        };
                        lobjCountry.Add(objectCountry);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjCountry;
        }
        #endregion



    }
}
