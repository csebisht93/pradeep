﻿using ContentManageApi.Entities.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContentManageApi.Entities
{
    public class CommonClass:BaseClass
    {
        public string Content { get; set; }
        public string AdditionalField { get; set; }


    }
}
