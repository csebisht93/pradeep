﻿using ContentManageApi.DataAccess.Mapper;
using ContentManageApi.DataAccess.Wrapper;
using ContentManageApi.Entities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContentManageApi.Business
{
    public class ContentFacade
    {
        ILoggerFactory _loggerFactory;

        #region Get Contact Details
        public List<CommonClass> GetContactDetails()
        {
            List<CommonClass> list = new List<CommonClass>();
            try
            {
                list = DataWrapper.MapGetContactDetails(SPWrapper.GetContactDetails());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }

        #endregion


        #region Get HomePage Details
        public List<CommonClass> GetHomePageDetails()
        {
            List<CommonClass> list = new List<CommonClass>();
            try
            {
                list = DataWrapper.MapGetHomePageDetails(SPWrapper.GetHomePageDetails());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }

        #endregion


        #region Get InfoPage Details
        public List<CommonClass> GetInfoPageDetails()
        {
            List<CommonClass> list = new List<CommonClass>();
            try
            {
                list = DataWrapper.MapGetInfoPageDetails(SPWrapper.GetInfoPageDetails());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion


        #region Get PlanPage Details
        public List<CommonClass> GetPlanPageDetails()
        {
            List<CommonClass> list = new List<CommonClass>();
            try
            {
                list = DataWrapper.MapGetPlanPageDetails(SPWrapper.GetPlanPageDetails());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #region Get FAQPage Details
        public List<CommonClass> GetFAQPageDetails()
        {
            List<CommonClass> list = new List<CommonClass>();
            try
            {
                list = DataWrapper.MapGetFAQPageDetails(SPWrapper.GetFAQPageDetails());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #region Get FooterPage Details
        public List<CommonClass> GetFooterPageDetails()
        {
            List<CommonClass> list = new List<CommonClass>();
            try
            {
                list = DataWrapper.MapGetFooterPageDetails(SPWrapper.GetFooterPageDetails());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #region Get HeaderPage Details
        public List<CommonClass> GetHeaderPageDetails()
        {
            List<CommonClass> list = new List<CommonClass>();
            try
            {
                list = DataWrapper.MapGetHeaderPageDetails(SPWrapper.GetHeaderPageDetails());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion
    }
}
