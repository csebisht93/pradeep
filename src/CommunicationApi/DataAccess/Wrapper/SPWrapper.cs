﻿using CommunicationApi.DataAccess.Constant;
using CommunicationApi.Entities;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace CommunicationApi.DataAccess.Wrapper
{
    public class SPWrapper
    {
        static ILoggerFactory _loggerFactory;
        public static string ConnectionString { get; set; }

        public SPWrapper(string connectionString)
        {
            ConnectionString = connectionString;
        }

        static MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }


        #region EmailTemplate section
        #region EmailTemplate list
        public static DataSet GetEmailTemplateList()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetEmailTemplateList, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #region Save EmailTemplate
        public static int InsertEmailTemplate(EmailTemplate pobjEmailTemplate)
        {
            int result = 0;
            MySqlDataAdapter da;
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertEmailTemplate, con);
           
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, pobjEmailTemplate.Id);
                cmd.Parameters.AddWithValue(DBFields.Name, pobjEmailTemplate.Name);
                cmd.Parameters.AddWithValue(DBFields.Description, pobjEmailTemplate.Description);
                cmd.Parameters.AddWithValue(DBFields.TemplateText, pobjEmailTemplate.TemplateText);
                cmd.Parameters.AddWithValue(DBFields.TemplateCode, pobjEmailTemplate.TemplateCode);
                cmd.Parameters.AddWithValue(DBFields.Subject, pobjEmailTemplate.Subject);

                cmd.Parameters.AddWithValue(DBFields.CreatedBy, pobjEmailTemplate.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, pobjEmailTemplate.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, pobjEmailTemplate.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, pobjEmailTemplate.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, pobjEmailTemplate.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, pobjEmailTemplate.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, pobjEmailTemplate.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, pobjEmailTemplate.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, pobjEmailTemplate.IsActive);


                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save EmailTemplate SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return result;
        }

        #endregion

        #region EmailTemplate Edit
        public static DataSet GetEmailTemplateDetailsById(int? EmailTemplateId)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetEmailTemplateDetailsById, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.Id, EmailTemplateId);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #endregion

        #region EmailConfig section
        #region EmailConfig list
        public static DataSet GetEmailConfigList()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetEmailConfigList, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #region Save EmailConfig
        public static int InsertEmailConfig(EmailConfig pobjEmailConfig)
        {
            int result = 0;
            MySqlDataAdapter da;
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertEmailConfig, con);
           
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, pobjEmailConfig.Id);
                cmd.Parameters.AddWithValue(DBFields.Name, pobjEmailConfig.Name);
                cmd.Parameters.AddWithValue(DBFields.UserName, pobjEmailConfig.UserName);
                cmd.Parameters.AddWithValue(DBFields.Password, pobjEmailConfig.Password);
                cmd.Parameters.AddWithValue(DBFields.PortNumber, pobjEmailConfig.PortNumber);
                cmd.Parameters.AddWithValue(DBFields.Server, pobjEmailConfig.Server);
                cmd.Parameters.AddWithValue(DBFields.From, pobjEmailConfig.From);
                cmd.Parameters.AddWithValue(DBFields.To, pobjEmailConfig.To);
                cmd.Parameters.AddWithValue(DBFields.Cc, pobjEmailConfig.Cc);
                cmd.Parameters.AddWithValue(DBFields.Bcc, pobjEmailConfig.Bcc);
                cmd.Parameters.AddWithValue(DBFields.ReplyAddress, pobjEmailConfig.ReplyAddress);
                cmd.Parameters.AddWithValue(DBFields.Ssl, pobjEmailConfig.Ssl);

                cmd.Parameters.AddWithValue(DBFields.CreatedBy, pobjEmailConfig.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, pobjEmailConfig.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, pobjEmailConfig.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, pobjEmailConfig.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, pobjEmailConfig.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, pobjEmailConfig.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, pobjEmailConfig.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, pobjEmailConfig.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, pobjEmailConfig.IsActive);


                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save EmailConfig SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return result;
        }

        #endregion

        #region EmailConfig Edit
        public static DataSet GetEmailConfigDetailsById(int? EmailConfigId)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetEmailConfigDetailsById, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.Id, EmailConfigId);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion
        #endregion


        #region SmsTemplate section
        #region SmsTemplate list
        public static DataSet GetSmsTemplateList()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetSmsTemplateList, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #region Save SmsTemplate
        public static int InsertSmsTemplate(SmsTemplate pobjSmsTemplate)
        {
            int result = 0;
            MySqlDataAdapter da;
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertSmsTemplate, con);

            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, pobjSmsTemplate.Id);
                cmd.Parameters.AddWithValue(DBFields.Name, pobjSmsTemplate.Name);
                cmd.Parameters.AddWithValue(DBFields.Description, pobjSmsTemplate.Description);
                cmd.Parameters.AddWithValue(DBFields.TemplateText, pobjSmsTemplate.TemplateText);
                cmd.Parameters.AddWithValue(DBFields.TemplateCode, pobjSmsTemplate.TemplateCode);
                cmd.Parameters.AddWithValue(DBFields.Subject, pobjSmsTemplate.Subject);

                cmd.Parameters.AddWithValue(DBFields.CreatedBy, pobjSmsTemplate.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, pobjSmsTemplate.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, pobjSmsTemplate.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, pobjSmsTemplate.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, pobjSmsTemplate.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, pobjSmsTemplate.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, pobjSmsTemplate.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, pobjSmsTemplate.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, pobjSmsTemplate.IsActive);


                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save SmsTemplate SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return result;
        }

        #endregion

        #region SmsTemplate Edit
        public static DataSet GetSmsTemplateDetailsById(int? SmsTemplateId)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetSmsTemplateDetailsById, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.Id, SmsTemplateId);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion
        #endregion


        #region SmsConfig section
        #region SmsConfig list
        public static DataSet GetSmsConfigList()
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetSmsConfigList, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion

        #region Save SmsConfig
        public static int InsertSmsConfig(SmsConfig pobjSmsConfig)
        {
            int result = 0;
            MySqlDataAdapter da;
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.InsertSmsConfig, con);

            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                cmd.Parameters.AddWithValue(DBFields.Id, pobjSmsConfig.Id);
                cmd.Parameters.AddWithValue(DBFields.Name, pobjSmsConfig.Name);
                cmd.Parameters.AddWithValue(DBFields.UserName, pobjSmsConfig.UserName);
                cmd.Parameters.AddWithValue(DBFields.Password, pobjSmsConfig.Password);
                cmd.Parameters.AddWithValue(DBFields.PortNumber, pobjSmsConfig.PortNumber);
                cmd.Parameters.AddWithValue(DBFields.Server, pobjSmsConfig.Server);
                cmd.Parameters.AddWithValue(DBFields.From, pobjSmsConfig.From);
                cmd.Parameters.AddWithValue(DBFields.To, pobjSmsConfig.To);
                cmd.Parameters.AddWithValue(DBFields.Cc, pobjSmsConfig.Cc);
                cmd.Parameters.AddWithValue(DBFields.Bcc, pobjSmsConfig.Bcc);
                cmd.Parameters.AddWithValue(DBFields.ReplyAddress, pobjSmsConfig.ReplyAddress);
                cmd.Parameters.AddWithValue(DBFields.Ssl, pobjSmsConfig.Ssl);

                cmd.Parameters.AddWithValue(DBFields.CreatedBy, pobjSmsConfig.CreatedBy);
                cmd.Parameters.AddWithValue(DBFields.CreatedOn, pobjSmsConfig.CreatedOn);
                cmd.Parameters.AddWithValue(DBFields.ModifiedBy, pobjSmsConfig.ModifiedBy);
                cmd.Parameters.AddWithValue(DBFields.ModifiedOn, pobjSmsConfig.ModifiedOn);
                cmd.Parameters.AddWithValue(DBFields.CreatedIP, pobjSmsConfig.CreatedIP);
                cmd.Parameters.AddWithValue(DBFields.ModifiedIP, pobjSmsConfig.ModifiedIP);
                cmd.Parameters.AddWithValue(DBFields.CreatedMacAddress, pobjSmsConfig.CreatedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.ModifiedMacAddress, pobjSmsConfig.ModifiedMacAddress);
                cmd.Parameters.AddWithValue(DBFields.IsActive, pobjSmsConfig.IsActive);


                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("save SmsConfig SP");
                logger.LogInformation(ex.Message);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return result;
        }

        #endregion

        #region SmsConfig Edit
        public static DataSet GetSmsConfigDetailsById(int? SmsConfigId)
        {
            DataSet ds = new DataSet();
            MySqlConnection con = GetConnection();
            MySqlCommand cmd = new MySqlCommand(StoredProcedures.GetSmsConfigDetailsById, con);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.Parameters.AddWithValue(DBFields.Id, SmsConfigId);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection = null;
            }
            return ds;
        }
        #endregion
        #endregion

    }
}
