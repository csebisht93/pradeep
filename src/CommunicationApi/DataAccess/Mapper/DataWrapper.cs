﻿using CommunicationApi.DataAccess.Constant;
using CommunicationApi.Entities;
using CommunicationApi.Entities.BaseClasses;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace CommunicationApi.DataAccess.Mapper
{
    public class DataWrapper
    {
        static ILoggerFactory _loggerFactory;

        public static ServiceResponse MapResponse(int result)
        {
            ServiceResponse response = new ServiceResponse();
            if (result > 0)
            {
                switch (result)
                {
                    case 0:
                        response.Errcode = result;
                        break;
                    default:
                        response.Errcode = 200;
                        break;
                }
                response.Id = result;
            }
            else
            {
                response.Errcode = 500;
                // response.Errcode = Convert.ToInt32(HelperConstants.ErrorCode.InternalError);
            }
            return response;
        }

        #region EmailTemplate section
        #region EmailTemplate list
        public static List<EmailTemplate> MapGetEmailTemplateList(DataSet Ds)
        {
            List<EmailTemplate> lobjEmailTemplate = new List<EmailTemplate>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjEmailTemplate = new List<EmailTemplate>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        EmailTemplate objectEmailTemplate = new EmailTemplate()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            Name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Name]),
                            Description = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Description]),
                            TemplateCode = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.TemplateCode]),
                            Subject = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Subject]),
                        };
                        lobjEmailTemplate.Add(objectEmailTemplate);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjEmailTemplate;
        }
        #endregion

        #region EmailTemplate edit
        public static List<EmailTemplate> MapGetEmailTemplateDetailsById(DataSet Ds)
        {
            List<EmailTemplate> lobjEmailTemplate = null;
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjEmailTemplate = new List<EmailTemplate>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        EmailTemplate objectEmailTemplate = new EmailTemplate()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            Name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Name]),
                            Description = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Description]),
                            TemplateCode = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.TemplateCode]),
                            Subject = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Subject]),

                        };

                        lobjEmailTemplate.Add(objectEmailTemplate);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjEmailTemplate;
        }
        #endregion

        #endregion

        #region EmailConfig section
        #region EmailConfig list
        public static List<EmailConfig> MapGetEmailConfigList(DataSet Ds)
        {
            List<EmailConfig> lobjEmailConfig = new List<EmailConfig>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjEmailConfig = new List<EmailConfig>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        EmailConfig objectEmailConfig = new EmailConfig()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            Name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Name]),
                            Server = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Server]),
                            From = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.From]),
                            To = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.To]),
                        };
                        lobjEmailConfig.Add(objectEmailConfig);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjEmailConfig;
        }
        #endregion

        #region EmailConfig edit
        public static List<EmailConfig> MapGetEmailConfigDetailsById(DataSet Ds)
        {
            List<EmailConfig> lobjEmailConfig = null;
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjEmailConfig = new List<EmailConfig>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        EmailConfig objectEmailConfig = new EmailConfig()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            Name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Name]),
                            UserName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.UserName]),
                            Password = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Password]),
                            PortNumber = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.PortNumber]),
                            Server = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Server]),                           
                            From = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.From]),
                            To = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.To]),
                            Cc = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Cc]),
                            Bcc = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Bcc]),
                            ReplyAddress = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ReplyAddress]),
                            Ssl = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Ssl]),

                        };

                        lobjEmailConfig.Add(objectEmailConfig);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjEmailConfig;
        }
        #endregion

        #endregion


        #region SmsTemplate section
        #region SmsTemplate list
        public static List<SmsTemplate> MapGetSmsTemplateList(DataSet Ds)
        {
            List<SmsTemplate> lobjSmsTemplate = new List<SmsTemplate>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjSmsTemplate = new List<SmsTemplate>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        SmsTemplate objectSmsTemplate = new SmsTemplate()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            Name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Name]),
                            Description = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Description]),
                            TemplateCode = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.TemplateCode]),
                            Subject = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Subject]),
                        };
                        lobjSmsTemplate.Add(objectSmsTemplate);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjSmsTemplate;
        }
        #endregion

        #region SmsTemplate edit
        public static List<SmsTemplate> MapGetSmsTemplateDetailsById(DataSet Ds)
        {
            List<SmsTemplate> lobjSmsTemplate = null;
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjSmsTemplate = new List<SmsTemplate>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        SmsTemplate objectSmsTemplate = new SmsTemplate()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            Name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Name]),
                            Description = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Description]),
                            TemplateCode = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.TemplateCode]),
                            Subject = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Subject]),
                        };
                        lobjSmsTemplate.Add(objectSmsTemplate);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjSmsTemplate;
        }
        #endregion
        #endregion


        #region SmsConfig section
        #region SmsConfig list
        public static List<SmsConfig> MapGetSmsConfigList(DataSet Ds)
        {
            List<SmsConfig> lobjSmsConfig = new List<SmsConfig>();
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjSmsConfig = new List<SmsConfig>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        SmsConfig objectSmsConfig = new SmsConfig()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            Name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Name]),
                            Server = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Server]),
                            From = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.From]),
                            To = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.To]),
                        };
                        lobjSmsConfig.Add(objectSmsConfig);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjSmsConfig;
        }
        #endregion

        #region SmsConfig edit
        public static List<SmsConfig> MapGetSmsConfigDetailsById(DataSet Ds)
        {
            List<SmsConfig> lobjSmsConfig = null;
            try
            {
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    lobjSmsConfig = new List<SmsConfig>();
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        SmsConfig objectSmsConfig = new SmsConfig()
                        {
                            Id = Convert.ToInt32(Ds.Tables[0].Rows[i][DBFields.Id]),
                            Name = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Name]),
                            UserName = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.UserName]),
                            Password = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Password]),
                            PortNumber = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.PortNumber]),
                            Server = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Server]),
                            From = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.From]),
                            To = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.To]),
                            Cc = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Cc]),
                            Bcc = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Bcc]),
                            ReplyAddress = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.ReplyAddress]),
                            Ssl = Convert.ToString(Ds.Tables[0].Rows[i][DBFields.Ssl]),

                        };

                        lobjSmsConfig.Add(objectSmsConfig);
                    }
                }
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            return lobjSmsConfig;
        }
        #endregion
        #endregion


    }
}
