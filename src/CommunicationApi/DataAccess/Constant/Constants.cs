﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommunicationApi.DataAccess.Constant
{
    public class DBFields
    {
        //-----------Common Base class field ---------------------------
        public static string Id = "id";
        public static string IsActive = "is_active";
        public static string CreatedBy = "created_by";
        public static string CreatedOn = "created_on";
        public static string ModifiedBy = "modified_by";
        public static string ModifiedOn = "modified_on";
        public static string CreatedIP = "ip_address";
        public static string ModifiedIP = "modified_ip";
        public static string CreatedMacAddress = "mac_address";
        public static string ModifiedMacAddress = "modified_mac";
        public static string ErrCode = "ErrCode";


        public static string Name = "name";
        public static string Description = "description";
        public static string TemplateText = "template_text";
        public static string TemplateCode = "template_code";
        public static string Subject = "subject";
        public static string Server = "server_name";
        public static string From = "from_email";
        public static string To = "to_email";

        public static string UserName = "user_name";
        public static string Password = "password";
        public static string PortNumber = "port_number";
        public static string Cc = "cc";
        public static string Bcc = "bcc";
        public static string ReplyAddress = "reply_address";
        public static string Ssl = "ssl_val";


    }



    public class StoredProcedures
    {
        public static string GetEmailTemplateList = "sp_GetEmailTemplateList";
        public static string InsertEmailTemplate = "sp_InsertEmailTemplate";
        public static string GetEmailTemplateDetailsById = "sp_GetEmailTemplateDetailsById";

        public static string GetEmailConfigList = "sp_GetEmailConfigList";
        public static string InsertEmailConfig = "sp_InsertEmailConfig";
        public static string GetEmailConfigDetailsById = "sp_GetEmailConfigDetailsById";



        public static string GetSmsTemplateList = "sp_GetSmsTemplateList";
        public static string InsertSmsTemplate = "sp_InsertSmsTemplate";
        public static string GetSmsTemplateDetailsById = "sp_GetSmsTemplateDetailsById";


        public static string GetSmsConfigList = "sp_GetSmsConfigList";
        public static string InsertSmsConfig = "sp_InsertSmsConfig";
        public static string GetSmsConfigDetailsById = "sp_GetSmsConfigDetailsById";



    }


}
