﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CommunicationApi.Business;
using CommunicationApi.Entities;
using CommunicationApi.Entities.BaseClasses;

namespace CommunicationApi.Controllers
{
    [Route("CommApi/[controller]/[action]")]
    [ApiController]
    public class SmsEngineController : ControllerBase
    {
        SmsEngineFacade objSMSFacade = new SmsEngineFacade();
        ILoggerFactory _loggerFactory;

     #region SmsTemplate section
       #region SmsTemplate list
        [HttpGet]
        public List<SmsTemplate> GetSmsTemplateList()
        {
            List<SmsTemplate> objSmsTemplate = new List<SmsTemplate>();
            try
            {
                objSmsTemplate = objSMSFacade.GetSmsTemplateList();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return objSmsTemplate;
        }
        #endregion



        #region insert SmsTemplate
        public ServiceResponse InsertSmsTemplate(SmsTemplate pobjSmsTemplate)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                response = objSMSFacade.InsertSmsTemplate(pobjSmsTemplate);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("Insert SmsTemplate M");
                logger.LogInformation(ex.Message);
            }
            return response;
        }
        #endregion

        #region SmsTemplate Edit
        [HttpGet]
        public List<SmsTemplate> GetSmsTemplateDetailsById(int? SmsTemplateId)
        {
            List<SmsTemplate> objSmsTemplate = new List<SmsTemplate>();
            try
            {
                objSmsTemplate = objSMSFacade.GetSmsTemplateDetailsById(SmsTemplateId);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return objSmsTemplate;
        }
        #endregion
        #endregion

     #region SmsConfig section
        #region SmsConfig list
        [HttpGet]
        public List<SmsConfig> GetSmsConfigList()
        {
            List<SmsConfig> objSmsConfig = new List<SmsConfig>();
            try
            {
                objSmsConfig = objSMSFacade.GetSmsConfigList();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return objSmsConfig;
        }
        #endregion



        #region insert SmsConfig
        public ServiceResponse InsertSmsConfig(SmsConfig pobjSmsConfig)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                response = objSMSFacade.InsertSmsConfig(pobjSmsConfig);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("Insert SmsConfig M");
                logger.LogInformation(ex.Message);
            }
            return response;
        }
        #endregion

        #region SmsConfig Edit
        [HttpGet]
        public List<SmsConfig> GetSmsConfigDetailsById(int? SmsConfigId)
        {
            List<SmsConfig> objSmsConfig = new List<SmsConfig>();
            try
            {
                objSmsConfig = objSMSFacade.GetSmsConfigDetailsById(SmsConfigId);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return objSmsConfig;
        }
        #endregion
 #endregion




    }
}