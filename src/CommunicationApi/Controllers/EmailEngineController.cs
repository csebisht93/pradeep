﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommunicationApi.Business;
using CommunicationApi.Entities;
using CommunicationApi.Entities.BaseClasses;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CommunicationApi.Controllers
{
    [Route("CommApi/[controller]/[action]")]
    [ApiController]
    public class EmailEngineController : ControllerBase
    {

        EmailEngineFacade objCommunicationFacade = new EmailEngineFacade();
        ILoggerFactory _loggerFactory;

        #region EmailTemplate section
        #region EmailTemplate list
        [HttpGet]
        public List<EmailTemplate> GetEmailTemplateList()
        {
            List<EmailTemplate> objEmailTemplate = new List<EmailTemplate>();
            try
            {
                objEmailTemplate = objCommunicationFacade.GetEmailTemplateList();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return objEmailTemplate;
        }
        #endregion



        #region insert EmailTemplate
        public ServiceResponse InsertEmailTemplate(EmailTemplate pobjEmailTemplate)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                response = objCommunicationFacade.InsertEmailTemplate(pobjEmailTemplate);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("Insert EmailTemplate M");
                logger.LogInformation(ex.Message);
            }
            return response;
        }
        #endregion

        #region EmailTemplate Edit
        [HttpGet]
        public List<EmailTemplate> GetEmailTemplateDetailsById(int? EmailTemplateId)
        {
            List<EmailTemplate> objEmailTemplate = new List<EmailTemplate>();
            try
            {
                objEmailTemplate = objCommunicationFacade.GetEmailTemplateDetailsById(EmailTemplateId);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return objEmailTemplate;
        }
        #endregion

        #endregion


        #region EmailConfig section
        #region EmailConfig list
        [HttpGet]
        public List<EmailConfig> GetEmailConfigList()
        {
            List<EmailConfig> objEmailConfig = new List<EmailConfig>();
            try
            {
                objEmailConfig = objCommunicationFacade.GetEmailConfigList();
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return objEmailConfig;
        }
        #endregion



        #region insert EmailConfig
        public ServiceResponse InsertEmailConfig(EmailConfig pobjEmailConfig)
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                response = objCommunicationFacade.InsertEmailConfig(pobjEmailConfig);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("Insert EmailConfig M");
                logger.LogInformation(ex.Message);
            }
            return response;
        }
        #endregion

        #region EmailConfig Edit
        [HttpGet]
        public List<EmailConfig> GetEmailConfigDetailsById(int? EmailConfigId)
        {
            List<EmailConfig> objEmailConfig = new List<EmailConfig>();
            try
            {
                objEmailConfig = objCommunicationFacade.GetEmailConfigDetailsById(EmailConfigId);
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);
            }
            finally
            {

            }
            return objEmailConfig;
        }
        #endregion

        #endregion


    }
}