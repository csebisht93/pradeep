﻿using CommunicationApi.DataAccess.Mapper;
using CommunicationApi.DataAccess.Wrapper;
using CommunicationApi.Entities;
using CommunicationApi.Entities.BaseClasses;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommunicationApi.Business
{
    public class EmailEngineFacade
    {
        ILoggerFactory _loggerFactory;

        #region EmailTemplate section
        #region EmailTemplate list
        public List<EmailTemplate> GetEmailTemplateList()
        {
            List<EmailTemplate> list = new List<EmailTemplate>();
            try
            {
                list = DataWrapper.MapGetEmailTemplateList(SPWrapper.GetEmailTemplateList());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #region EmailTemplate save
        public ServiceResponse InsertEmailTemplate(EmailTemplate pobjEmailTemplate)
        {
            return DataWrapper.MapResponse(SPWrapper.InsertEmailTemplate(pobjEmailTemplate));
        }
        #endregion

        #region EmailTemplate Edit
        public List<EmailTemplate> GetEmailTemplateDetailsById(int? EmailTemplateId)
        {
            List<EmailTemplate> list = null;
            try
            {
                list = DataWrapper.MapGetEmailTemplateDetailsById(SPWrapper.GetEmailTemplateDetailsById(EmailTemplateId));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #endregion

        #region EmailConfig section
        #region EmailConfig list
        public List<EmailConfig> GetEmailConfigList()
        {
            List<EmailConfig> list = new List<EmailConfig>();
            try
            {
                list = DataWrapper.MapGetEmailConfigList(SPWrapper.GetEmailConfigList());
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #region EmailConfig save
        public ServiceResponse InsertEmailConfig(EmailConfig pobjEmailConfig)
        {
            return DataWrapper.MapResponse(SPWrapper.InsertEmailConfig(pobjEmailConfig));
        }
        #endregion

        #region EmailConfig Edit
        public List<EmailConfig> GetEmailConfigDetailsById(int? EmailConfigId)
        {
            List<EmailConfig> list = null;
            try
            {
                list = DataWrapper.MapGetEmailConfigDetailsById(SPWrapper.GetEmailConfigDetailsById(EmailConfigId));
            }
            catch (Exception ex)
            {
                var logger = _loggerFactory.CreateLogger("LoginCategory");
                logger.LogInformation(ex.Message);

            }

            return list;
        }
        #endregion

        #endregion



    }
}
