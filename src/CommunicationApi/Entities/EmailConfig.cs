﻿using CommunicationApi.Entities.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommunicationApi.Entities
{
    public class EmailConfig:BaseClass
    {
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string PortNumber { get; set; }
        public string Server { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Cc { get; set; }
        public string Bcc { get; set; }
        public string ReplyAddress { get; set; }
        public string Ssl { get; set; }

    }
}
