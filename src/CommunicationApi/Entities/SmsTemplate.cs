﻿using CommunicationApi.Entities.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommunicationApi.Entities
{
    public class SmsTemplate:BaseClass
    {
        public string ConfigId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string TemplateText { get; set; }
        public string TemplateCode { get; set; }
        public string Subject { get; set; }

    }
}
