﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommunicationApi.Entities.BaseClasses
{
    public class ServiceResponse
    {
        public int Id { get; set; }
        public int Errcode { get; set; }
        public string Errdesc { get; set; }
        public Object ObjectParam { get; set; }
        public string AdditionalParam { get; set; }
    }
}
